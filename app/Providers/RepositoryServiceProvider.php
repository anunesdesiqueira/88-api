<?php

namespace OitentaOito\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\OitentaOito\Repositories\StateRepository::class,
            \OitentaOito\Repositories\StateRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\CityRepository::class,
            \OitentaOito\Repositories\CityRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\SectorRepository::class,
            \OitentaOito\Repositories\SectorRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\TaxFrameworkRepository::class,
            \OitentaOito\Repositories\TaxFrameworkRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\PositionJobRepository::class,
            \OitentaOito\Repositories\PositionJobRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\DepartmentRepository::class,
            \OitentaOito\Repositories\DepartmentRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\EducationLevelRepository::class,
            \OitentaOito\Repositories\EducationLevelRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\UserRepository::class,
            \OitentaOito\Repositories\UserRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\UserProfileRepository::class,
            \OitentaOito\Repositories\UserProfileRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\CompanyRepository::class,
            \OitentaOito\Repositories\CompanyRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\SubsidiaryRepository::class,
            \OitentaOito\Repositories\SubsidiaryRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\InvitationRepository::class,
            \OitentaOito\Repositories\InvitationRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\UserAdditionalRepository::class,
            \OitentaOito\Repositories\UserAdditionalRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\TypeExpenditureRepository::class,
            \OitentaOito\Repositories\TypeExpenditureRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\RefundRepository::class,
            \OitentaOito\Repositories\RefundRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\VacationRepository::class,
            \OitentaOito\Repositories\VacationRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\LicenseRepository::class,
            \OitentaOito\Repositories\LicenseRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\AbsenceRepository::class,
            \OitentaOito\Repositories\AbsenceRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\ProviderCategoryRepository::class,
            \OitentaOito\Repositories\ProviderCategoryRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\ProviderRepository::class,
            \OitentaOito\Repositories\ProviderRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\BenefitCategoryRepository::class,
            \OitentaOito\Repositories\BenefitCategoryRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\BenefitRepository::class,
            \OitentaOito\Repositories\BenefitRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\MediaRepository::class,
            \OitentaOito\Repositories\MediaRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\DocumentRepository::class,
            \OitentaOito\Repositories\DocumentRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\HourlyWorkloadRepository::class,
            \OitentaOito\Repositories\HourlyWorkloadRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\CalendarRepository::class,
            \OitentaOito\Repositories\CalendarRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\RemunerationVariableRepository::class,
            \OitentaOito\Repositories\RemunerationVariableRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\RemunerationRepository::class,
            \OitentaOito\Repositories\RemunerationRepositoryEloquent::class
        );

        $this->app->bind(\OitentaOito\Repositories\NotificationRepository::class,
            \OitentaOito\Repositories\NotificationRepositoryEloquent::class
        );
    }
}