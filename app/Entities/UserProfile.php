<?php

namespace OitentaOito\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OitentaOito\Traits\AddressTrait;
use OitentaOito\Traits\EmailTrait;
use OitentaOito\Traits\PhoneTrait;

class UserProfile extends Model
{
    use SoftDeletes, AddressTrait, PhoneTrait, EmailTrait;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_profiles';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];


    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'gender',
        'date_of_birth',
        'identity_document',
        'identity_document_state',
        'identity_document_dispatcher_organ',
        'identity_document_dispatcher_date',
        'register_individual',
        'voter_id_card',
        'driving_licence',
        'driving_licence_category',
        'driving_licence_dispatcher_date',
        'driving_licence_validate_date',
        'work_record_booklet',
        'work_record_booklet_series',
        'work_record_booklet_dispatcher_state',
        'social_integration_program',
        'professional_identity',
        'address',
        'data_address',
        'data_email',
        'data_phone',
        'data_phone_emergency',
        'about_me',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('OitentaOito\Entities\User');
    }


    /**
     * @param $value
     */
    public function setDateOfBirthAttribute($value) {
        $this->attributes['date_of_birth'] = Carbon::parse($value)->format('Y-m-d');
    }


    /**
     * @param $value
     */
    public function setIdentityDocumentDispatcherDateAttribute($value) {
        $this->attributes['identity_document_dispatcher_date'] = Carbon::parse($value)->format('Y-m-d');
    }


    /**
     * @param $value
     */
    public function setDrivingLicenceDispatcherDateAttribute($value) {
        $this->attributes['driving_licence_dispatcher_date'] = Carbon::parse($value)->format('Y-m-d');
    }


    /**
     * @param $value
     */
    public function setDrivingLicenceValidateDateAttribute($value) {
        $this->attributes['driving_licence_validate_date'] = Carbon::parse($value)->format('Y-m-d');
    }


    /**
     * @param $value
     */
    public function setProfessionalIdentityAttribute($value) {
        $this->attributes['professional_identity'] = json_encode($value);
    }


    /**
     * @param $value
     * @return mixed
     */
    public function getProfessionalIdentityAttribute($value) {
        return json_decode($value);
    }
}