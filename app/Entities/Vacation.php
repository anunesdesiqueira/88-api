<?php

namespace OitentaOito\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OitentaOito\Traits\CreatedUserIdTrait;
use OitentaOito\Traits\PeriodTrait;

class Vacation extends Model
{
    use SoftDeletes, PeriodTrait, CreatedUserIdTrait;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vacations';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];


    /**
     * @var array
     */
    protected $fillable = [
        'type',
        'created_user_id',
        'company_id',
        'user_id',
        'department_id',
        'vacation_bonus_days',
        'name',
        'status',
        'starting',
        'ending',
        'days',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('OitentaOito\Entities\Company');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo('OitentaOito\Entities\User');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo('OitentaOito\Entities\Department');
    }

}
