<?php

namespace OitentaOito\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OitentaOito\Traits\CreatedUserIdTrait;


class Document extends Model
{
    use SoftDeletes, CreatedUserIdTrait;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'documents';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];


    /**
     * @var array
     */
    protected $fillable = [
        'created_user_id',
        'company_id',
        'user_id',
        'name',
        'folder',
        'access',
    ];


    /**
     * @return mixed
     */
    public function file()
    {
        return $this->hasOne( 'OitentaOito\Entities\Media' , 'reference', 'id' )->where('slug', 'document_file')->orderBy('id', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo('OitentaOito\Entities\User');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('OitentaOito\Entities\Company');
    }

}
