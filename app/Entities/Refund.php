<?php

namespace OitentaOito\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OitentaOito\Traits\CreatedUserIdTrait;

class Refund extends Model
{
    use SoftDeletes, CreatedUserIdTrait;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'refunds';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];


    /**
     * @var array
     */
    protected $fillable = [
        'created_user_id',
        'user_id',
        'company_id',
        'type_expenditure_id',
        'status',
        'date',
        'value',
        'amount_approved',
        'description',
        'justification',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo('OitentaOito\Entities\User');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('OitentaOito\Entities\Company');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type()
    {
        return $this->belongsTo('OitentaOito\Entities\TypeExpenditure');
    }


    /**
     * @return mixed
     */
    public function fileReturnable()
    {
        return $this->hasOne( 'OitentaOito\Entities\Media' , 'reference', 'id' )->where('slug', 'refund_returnable_file')->orderBy('id', 'desc');
    }


    /**
     * @return mixed
     */
    public function fileReceipt()
    {
        return $this->hasOne( 'OitentaOito\Entities\Media' , 'reference', 'id' )->where('slug', 'refund_receipt_file')->orderBy('id', 'desc');
    }


    /**
     * @param $value
     */
    public function setDateAttribute($value) {
        $this->attributes['date'] = Carbon::parse($value)->format('Y-m-d');
    }
}
