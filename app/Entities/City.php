<?php

namespace OitentaOito\Entities;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cities';


    /**
     * @var array
     */
    protected $fillable = [
        'name', 'state_id'
    ];

}
