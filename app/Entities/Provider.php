<?php

namespace OitentaOito\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OitentaOito\Traits\AddressTrait;
use OitentaOito\Traits\CreatedUserIdTrait;
use OitentaOito\Traits\EmailTrait;
use OitentaOito\Traits\PhoneTrait;

class Provider extends Model
{
    use SoftDeletes, AddressTrait, PhoneTrait, EmailTrait, CreatedUserIdTrait;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'providers';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];


    /**
     * @var array
     */
    protected $fillable = [
        'created_user_id',
        'company_id',
        'provider_category_id',
        'name',
        'contact_name',
        'data_phone',
        'data_email',
        'address',
        'data_address',
        'benefit'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('OitentaOito\Entities\Company');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('OitentaOito\Entities\ProviderCategory');
    }
}
