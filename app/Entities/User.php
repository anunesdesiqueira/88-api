<?php

namespace OitentaOito\Entities;

use Illuminate\Foundation\Auth\User as Authenticatable;
use OitentaOito\Traits\RoleUserTrait;

class User extends Authenticatable
{
    use RoleUserTrait;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'google_id', 'terms_of_use', 'activation_code', 'activated',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function companies()
    {
        return $this->belongsToMany('\OitentaOito\Entities\Company', 'role_user', 'user_id', 'company_id')
            ->withPivot('role_id')
            ->withTimestamps();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne('OitentaOito\Entities\UserProfile');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function additional()
    {
        return $this->hasMany('OitentaOito\Entities\UserAdditional');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roleUser()
    {
        return $this->hasMany('OitentaOito\Entities\RoleUser');
    }


    /**
     * @return mixed
     */
    public function avatar()
    {
        return $this->hasOne('OitentaOito\Entities\Media', 'reference', 'id')->where('slug', 'user_avatar')->orderBy('id', 'desc');
    }
}
