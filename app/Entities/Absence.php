<?php

namespace OitentaOito\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OitentaOito\Traits\CreatedUserIdTrait;
use OitentaOito\Traits\PeriodTrait;

class Absence extends Model
{
    use SoftDeletes, PeriodTrait, CreatedUserIdTrait;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'absences';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];


    /**
     * @var array
     */
    protected $fillable = [
        'created_user_id',
        'user_id',
        'company_id',
        'status',
        'starting',
        'ending',
        'comment',
        'justified',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo('OitentaOito\Entities\User');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('OitentaOito\Entities\Company');
    }


    /**
     * @return mixed
     */
    public function file()
    {
        return $this->hasOne( 'OitentaOito\Entities\Media' , 'reference', 'id' )->where('slug', 'absence_file')->orderBy('id', 'desc');
    }
}
