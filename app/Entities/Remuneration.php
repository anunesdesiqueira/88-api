<?php

namespace OitentaOito\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OitentaOito\Traits\CreatedUserIdTrait;

class Remuneration extends Model
{
    use SoftDeletes, CreatedUserIdTrait;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'remunerations';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];


    /**
     * @var array
     */
    protected $fillable = [
        'created_user_id',
        'user_id',
        'company_id',
        'name',
        'payday',
        'base_salary',
        'benefits',
        'discounts',
        'net_salary',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo('OitentaOito\Entities\User');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('OitentaOito\Entities\Company');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function variables()
    {
        return $this->belongsToMany('\OitentaOito\Entities\RemunerationVariable', 'remuneration_has_variables', 'remuneration_id', 'remuneration_variable_id')->withPivot('value_or_amount');
    }


    /**
     * @return mixed
     */
    public function payslip()
    {
        return $this->hasOne( 'OitentaOito\Entities\Media' , 'reference', 'id' )->where('slug', 'remuneration_file')->orderBy('id', 'desc');
    }

}
