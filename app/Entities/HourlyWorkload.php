<?php

namespace OitentaOito\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OitentaOito\Traits\CreatedUserIdTrait;

class HourlyWorkload extends Model
{
    use SoftDeletes, CreatedUserIdTrait;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hourly_workloads';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];


    /**
     * @var array
     */
    protected $fillable = [
        'created_user_id',
        'company_id',
        'name',
        'data_week',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->belongsToMany('OitentaOito\Entities\User', 'hourly_workload_has_users', 'hourly_workload_id', 'user_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('OitentaOito\Entities\Company');
    }


    /**
     * @param $value
     * @return mixed
     */
    public function getDataWeekAttribute($value) {
        return json_decode($value);
    }


    /**
     * @param $value
     */
    public function setDataWeekAttribute($value) {
        $this->attributes['data_week'] = json_encode($value);
    }

}
