<?php

namespace OitentaOito\Entities;

use Illuminate\Database\Eloquent\Model;

class SalaryTaxes extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'salary_taxes';


    /**
     * @var array
     */
    protected $fillable = [
        'type',
        'rules',
    ];

}
