<?php

namespace OitentaOito\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OitentaOito\Traits\SlugTrait;

class ProviderCategory extends Model
{
    use SoftDeletes, SlugTrait;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'providers_categories';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];


    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
    ];
}
