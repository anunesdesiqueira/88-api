<?php

namespace OitentaOito\Entities;

use Illuminate\Database\Eloquent\Model;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\Factories\Attachment as AttachmentFactory;
use Illuminate\Support\Str;
use OitentaOito\Traits\CreatedUserIdTrait;

class Media extends Model implements StaplerableInterface
{

    use EloquentTrait, CreatedUserIdTrait;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'medias';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];


    /**
     * @var array
     */
    protected $fillable = [
        'created_user_id',
        'company_id',
        'user_id',
        'reference',
        'slug',
        'name',
        'folder',
        'access',
        'file',
    ];


    /**
     * Media constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('file', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ],
        ]);

        parent::__construct($attributes);
    }


    /**
     * Handle the dynamic setting of attachment objects.
     *
     * @param string $key
     * @param mixed  $value
     * @return void
     */
    public function setAttribute($key, $value)
    {
        if (array_key_exists($key, $this->attachedFiles)) {
            if ($value) {

                $pathInfo = pathinfo($value->getClientOriginalName());

                if(!in_array($pathInfo['extension'], ['jpeg','jpg','png','bmp','gif'])) {
                    $attachment = AttachmentFactory::create($key, []);
                    $attachment->setInstance($this);
                    $this->attachedFiles[$key] = $attachment;
                }

                $attachedFile = $this->attachedFiles[$key];
                $attachedFile->setUploadedFile($value);
            }

            return;
        }

        parent::setAttribute($key, $value);
    }


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        // Call the bootStapler() method to register stapler as an observer for this model.
        static::bootStapler();


        // Now, before the record is saved, set the filename attribute on the model:
        static::saving(function($model)
        {
            $pathInfo = pathinfo($model->file->originalFileName());
            $filename = (!is_null($model->name) && !empty($model->name)? $model->name : $pathInfo['filename']);
            $newFilename = Str::slug($filename) . '.' . $pathInfo['extension'];
            $model->file->instanceWrite('file_name', $newFilename);
        });
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo('OitentaOito\Entities\User');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('OitentaOito\Entities\Company');
    }


    /**
     * @param $ids
     * @param $reference
     * @return mixed
     */
    public static function setMyReference($ids , $reference)
    {
        if(!is_array($ids))
            $ids = [$ids];

        return self::whereIn("id",$ids)->update(["reference" => $reference]);
    }
}
