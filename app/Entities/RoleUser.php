<?php

namespace OitentaOito\Entities;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'role_user';


    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_id',
        'role_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('OitentaOito\Entities\Company');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('OitentaOito\Entities\Role');
    }
}
