<?php

namespace OitentaOito\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OitentaOito\Traits\AddressTrait;
use OitentaOito\Traits\EmailTrait;
use OitentaOito\Traits\PhoneTrait;
use OitentaOito\Traits\SlugTrait;

class Company extends Model
{
    use SoftDeletes, AddressTrait, PhoneTrait, EmailTrait, SlugTrait;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];


    /**
     * @var array
     */
    protected $fillable = [
        'parent',
        'type',
        'slug',
        'trading_name',
        'company_name',
        'national_register_of_legal_entities',
        'state_registration',
        'sector_id',
        'sector_name',
        'tax_framework_id',
        'address',
        'data_address',
        'data_email',
        'data_phone',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('\OitentaOito\Entities\User', 'role_user', 'company_id', 'user_id')
            ->withPivot('role_id')
            ->withTimestamps();
    }


    /**
     * @return mixed
     */
    public function avatar()
    {
        return $this->hasOne('OitentaOito\Entities\Media', 'reference', 'id')->where('slug', 'company_avatar')->orderBy('id', 'desc');
    }
}
