<?php

namespace OitentaOito\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Validator\Contracts\ValidatorInterface;

class UserAdditional extends Model
{
    use SoftDeletes;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_additionals';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];


    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_id',
        'department_id',
        'position_job_id',
        'education_level_id',
        'admission_date',
        'vacation_balance_days',
        'schooling',
        'base_salary',
        'payday',
        'bank_data',
        'special_needs',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function department()
    {
        return $this->belongsTo('OitentaOito\Entities\Department');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function positionJob()
    {
        return $this->belongsTo('OitentaOito\Entities\PositionJob');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function educationLevel()
    {
        return $this->belongsTo('OitentaOito\Entities\EducationLevel');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function discounts()
    {
        return $this->belongsToMany('\OitentaOito\Entities\Benefit', 'user_has_discounts', 'user_additional_id', 'benefit_id')->withPivot('type', 'value');
    }


    /**
     * @param $value
     */
    public function setAdmissionDateAttribute($value) {
        $this->attributes['admission_date'] = Carbon::parse($value)->format('Y-m-d');
    }


    /**
     * @param $value
     */
    public function setBankDataAttribute($value) {
        $this->attributes['bank_data'] = json_encode($value);
    }


    /**
     * @param $value
     * @return mixed
     */
    public function getBankDataAttribute($value) {
        return json_decode($value);
    }


    /**
     * @param $value
     */
    public function setPaydayAttribute($value) {
        $this->attributes['payday'] = json_encode($value);
    }


    /**
     * @param $value
     * @return mixed
     */
    public function getPaydayAttribute($value) {
        return json_decode($value);
    }


    /**
     * @param $value
     */
    public function setSpecialNeedsAttribute($value) {
        $this->attributes['special_needs'] = json_encode($value);
    }


    /**
     * @param $value
     * @return mixed
     */
    public function getSpecialNeedsAttribute($value) {
        return json_decode($value);
    }
}