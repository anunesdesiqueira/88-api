<?php

namespace OitentaOito\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

/**
 * Class DocumentCriteria
 * @package namespace OitentaOito\Criteria;
 */
class DocumentCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('user_id','=', Authorizer::getResourceOwnerId())
                       ->orWhere('access', 'public');

        return $model;
    }
}
