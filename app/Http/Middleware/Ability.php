<?php

namespace OitentaOito\Http\Middleware;

use Closure;
use OitentaOito\Services\ResponseService;

class Ability
{
    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * Ability constructor.
     * @param ResponseService $response
     */
    public function __construct(ResponseService $response)
    {
        $this->response = $response;
    }


    /**
     * @param $request
     * @param Closure $next
     * @param $roles
     * @param null $permissions
     * @param bool $validateAll
     * @return mixed
     */
    public function handle($request, Closure $next, $roles, $permissions = null, $validateAll = false)
    {
        $companyId = $request->company_id;
        $user = $request->user();

        if(!$user->ability($companyId, explode('|', $roles), explode('|', $permissions), array('validate_all' => $validateAll))) {
            return $this->response->errorUnauthorized();
        }

        return $next($request);
    }
}
