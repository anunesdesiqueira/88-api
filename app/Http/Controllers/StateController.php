<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\StateService;

class StateController extends Controller
{
    /**
     * @var StateService
     */
    protected $service;

    /**
     * StateController constructor.
     * @param StateService $service
     */
    public function __construct(StateService $service)
    {
        $this->service  = $service;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return $this->service->all($request->all());
    }
}
