<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\UserService;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    protected $service;


    /**
     * UserController constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service  = $service;
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \OitentaOito\Services\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit');
        return $this->service->all($limit);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \OitentaOito\Services\Response
     */
    public function show($id)
    {
        return $this->service->find($id);
    }

    /**
     * Display the auth specified resource.
     *
     *
     * @return \OitentaOito\Services\Response
     */
    public function auth()
    {
        return $this->service->findAuth();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  string            $id
     *
     * @return \OitentaOito\Services\Response
     */
    public function update(Request $request, $id)
    {
        return $this->service->update($request->all(), $id);
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \OitentaOito\Services\Response
     */
    public function destroy($id)
    {
        return $this->service->delete($id);
    }
}
