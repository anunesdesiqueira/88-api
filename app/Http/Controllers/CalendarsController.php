<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\CalendarService;


class CalendarsController extends Controller
{
    /**
     * @var CalendarService
     */
    protected $service;


    /**
     * CalendarsController constructor.
     * @param CalendarService $service
     */
    public function __construct(CalendarService $service)
    {
        $this->service = $service;
    }


    /**
     * @param Request $request
     * @param $companyId
     * @return \OitentaOito\Services\Response
     */
    public function index(Request $request, $companyId)
    {
        return $this->service->all($companyId, $request->all());
    }

}
