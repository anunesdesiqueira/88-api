<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\OAuthService;

class OAuthController extends Controller
{
    /**
     * @var OAuthService
     */
    protected $service;


    /**
     * OAuthController constructor.
     * @param OAuthService $service
     */
    public function __construct(OAuthService $service)
    {
        $this->service  = $service;
    }


    /**
     * @param $activationCode
     * @return \OitentaOito\Services\Response
     */
    public function activationCode($activationCode)
    {
//        return $this->service->activationCode($activationCode);
        $this->service->activationCode($activationCode);
//        return \Response::url("http://88.app.dbr.ag");
        return \Response::make( '', 302 )->header( 'Location', env('FRONT_URL') . "/auth/login" );
    }


    /**
     * @return \OitentaOito\Services\Response
     */
    public function accessToken()
    {
        return $this->service->accessToken();
    }


    /**
     * @return \OitentaOito\Services\Response
     */
    public function expireToken()
    {
        return $this->service->expireToken();
    }
}
