<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\RemunerationService;

class RemunerationController extends Controller
{
    /**
     * @var RemunerationService
     */
    protected $service;


    /**
     * RemunerationsController constructor.
     * @param RemunerationService $service
     */
    public function __construct(RemunerationService $service)
    {
        $this->service = $service;
    }


    /**
     * @param Request $request
     * @param $companyId
     * @return \OitentaOito\Services\Response
     */
    public function index(Request $request, $companyId)
    {
        $data = [
            'limit' => $request->input('limit'),
            'fromDate' => $request->input('fromDate'),
            'toDate' => $request->input('toDate'),
        ];

        return $this->service->all($companyId, $data);
    }


    /**
     * @param Request $request
     * @param $companyId
     * @return \Illuminate\Http\JsonResponse|\OitentaOito\Services\Response
     */
    public function store(Request $request, $companyId)
    {
        return $this->service->create($companyId, $request->all());
    }


    /**
     * @param $companyId
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function show($companyId, $id)
    {
        return $this->service->find($companyId, $id);
    }


    /**
     * @param Request $request
     * @param $companyId
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function update(Request $request, $companyId, $id)
    {
        return $this->service->update($companyId, $id, $request->all());
    }


    /**
     * @param $companyId
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function destroy($companyId, $id)
    {
        return $this->service->delete($companyId, $id);
    }
}
