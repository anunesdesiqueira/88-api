<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\TypeExpenditureService;

class TypeExpenditureController extends Controller
{
    /**
     * @var TypeExpenditureService
     */
    protected $service;


    /**
     * TypeExpenditureController constructor.
     * @param TypeExpenditureService $service
     */
    public function __construct(TypeExpenditureService $service)
    {
        $this->service = $service;
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit');
        return $this->service->all($limit);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\OitentaOito\Services\Response
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }


    /**
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function show($id)
    {
        return $this->service->find($id);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function update(Request $request, $id)
    {
        return $this->service->update($request->all(), $id);
    }


    /**
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function destroy($id)
    {
        return $this->service->delete($id);
    }
}
