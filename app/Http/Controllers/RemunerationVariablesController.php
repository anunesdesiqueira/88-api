<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\RemunerationVariableService;

class RemunerationVariablesController extends Controller
{
    /**
     * @var RemunerationVariableService
     */
    protected $service;


    /**
     * RemunerationVariablesController constructor.
     * @param RemunerationVariableService $service
     */
    public function __construct(RemunerationVariableService $service)
    {
        $this->service = $service;
    }


    /**
     * @param $companyId
     * @return \OitentaOito\Services\Response
     */
    public function index($companyId)
    {
        return $this->service->all($companyId);
    }


    /**
     * @param Request $request
     * @param $companyId
     * @return \Illuminate\Http\JsonResponse|\OitentaOito\Services\Response
     */
    public function store(Request $request, $companyId)
    {
        return $this->service->create($companyId, $request->all());
    }


    /**
     * @param $companyId
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function show($companyId, $id)
    {
        return $this->service->find($companyId, $id);
    }


    /**
     * @param Request $request
     * @param $companyId
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function update(Request $request, $companyId, $id)
    {
        return $this->service->update($companyId, $id, $request->all());
    }


    /**
     * @param $companyId
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function destroy($companyId, $id)
    {
        return $this->service->delete($companyId, $id);
    }
}
