<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\MediaService;

class MediaController extends Controller
{
    /**
     * @var MediaService
     */
    protected $service;


    /**
     * MediaController constructor.
     * @param MediaService $service
     */
    public function __construct(MediaService $service)
    {
        $this->service = $service;
    }


    /**
     * @param Request $request
     * @return \OitentaOito\Services\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit');
        return $this->service->all($limit);
    }


    /**
     * @param Request $request
     * @return \OitentaOito\Services\Response
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }


    /**
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function show($id)
    {
        return $this->service->find($id);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function update(Request $request, $id)
    {
        return $this->service->update($request->all(), $id);
    }


    /**
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function destroy($id)
    {
        return $this->service->delete($id);
    }
}