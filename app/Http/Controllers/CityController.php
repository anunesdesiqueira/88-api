<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\CityService;

class CityController extends Controller
{
    /**
     * @var CityService
     */
    protected $service;

    /**
     * CityController constructor.
     * @param CityService $service
     */
    public function __construct(CityService $service)
    {
        $this->service  = $service;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return $this->service->all($request->all());
    }
}
