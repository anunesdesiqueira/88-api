<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\SectorService;


class SectorController extends Controller
{
    /**
     * @var SectorService
     */
    protected $service;

    /**
     * SectorController constructor.
     * @param SectorService $service
     */
    public function __construct(SectorService $service)
    {
        $this->service = $service;
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit');
        return $this->service->all($limit);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\OitentaOito\Services\Response
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }


    /**
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function show($id)
    {
        return $this->service->find($id);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function update(Request $request, $id)
    {
        return $this->service->update($request->all(), $id);
    }


    /**
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function destroy($id)
    {
        return $this->service->delete($id);
    }
}
