<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\TeamService;

class TeamController extends Controller
{
    /**
     * @var TeamService
     */
    protected $service;


    /**
     * TeamController constructor.
     * @param TeamService $service
     */
    public function __construct(TeamService $service)
    {
        $this->service  = $service;
    }


    /**
     * @param Request $request
     * @param $companyId
     * @return \OitentaOito\Services\Response
     */
    public function index(Request $request, $companyId)
    {
        $limit = $request->input('limit');
        return $this->service->all($limit, $companyId);
    }
}
