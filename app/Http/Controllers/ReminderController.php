<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\ReminderService;

class ReminderController extends Controller
{
    /**
     * @var ReminderService
     */
    protected $service;


    /**
     * ReminderController constructor.
     * @param ReminderService $service
     */
    public function __construct(ReminderService $service)
    {
        $this->service = $service;
    }


    /**
     * @param Request $request
     * @return \OitentaOito\Services\Response
     */
    public function forgot(Request $request)
    {
        return $this->service->forgot($request->input('email'));
    }


    /**
     * @param Request $request
     * @return \OitentaOito\Services\Response
     */
    public function reset(Request $request)
    {
        return $this->service->reset($request->all());
    }

}
