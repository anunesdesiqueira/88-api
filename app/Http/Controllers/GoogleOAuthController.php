<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\GoogleOAuthService;

class GoogleOAuthController extends Controller
{
    /**
     * @var GoogleOAuthService
     */
    protected $service;


    /**
     * GoogleOAuthController constructor.
     * @param GoogleOAuthService $service
     */
    public function __construct(GoogleOAuthService $service)
    {
        $this->service  = $service;
    }


    /**
     * Redirect the user to the Google authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return $this->service->redirect();
    }


    /**
     * @return \OitentaOito\Services\Response
     */
    public function handleProviderCallback()
    {
        return $this->service->callback();
    }
}
