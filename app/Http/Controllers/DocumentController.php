<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\DocumentService;


class DocumentController extends Controller
{
    /**
     * @var DocumentService
     */
    protected $service;


    /**
     * DocumentsController constructor.
     * @param DocumentService $service
     */
    public function __construct(DocumentService $service)
    {
        $this->service = $service;
    }


    /**
     * @param Request $request
     * @param $companyId
     * @return \OitentaOito\Services\Response
     */
    public function index(Request $request, $companyId)
    {
        $limit = $request->input('limit');
        return $this->service->all($companyId, $limit);
    }


    /**
     * @param Request $request
     * @param $companyId
     * @return \OitentaOito\Services\Response
     */
    public function store(Request $request, $companyId)
    {
        return $this->service->create($companyId, $request->all());
    }


    /**
     * @param $companyId
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function show($companyId, $id)
    {
        return $this->service->find($companyId, $id);
    }


    /**
     * @param Request $request
     * @param $companyId
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function update(Request $request, $companyId, $id)
    {
        return $this->service->update($companyId, $id, $request->all());
    }


    /**
     * @param $companyId
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function destroy($companyId, $id)
    {
        return $this->service->delete($companyId, $id);
    }
}
