<?php

namespace OitentaOito\Http\Controllers;

use Illuminate\Http\Request;
use OitentaOito\Services\CompanyService;

class CompanyController extends Controller
{

    /**
     * @var CompanyService
     */
    protected $service;


    /**
     * CompanyController constructor.
     * @param CompanyService $service
     */
    public function __construct(CompanyService $service)
    {
        $this->service = $service;
    }


    /**
     * @param Request $request
     * @return \OitentaOito\Services\Response
     */
    public function index(Request $request)
    {
        return $this->service->all();
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\OitentaOito\Services\Response
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }


    /**
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function show($id)
    {
        return $this->service->find($id);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function update(Request $request, $id)
    {
        return $this->service->update($request->all(), $id);
    }


    /**
     * @param $id
     * @return \OitentaOito\Services\Response
     */
    public function destroy($id)
    {
        return $this->service->delete($id);
    }
}
