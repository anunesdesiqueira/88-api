<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'v1'], function () {

    Route::group(['middleware' => ['access']], function () {

        Route::group(['as' => 'helpers::'], function () {
            Route::get('states', 'StateController@index');
            Route::get('cities', 'CityController@index');
            Route::get('expenditures', 'TypeExpenditureController@index');
        });

        Route::group(['as' => 'oauth::', 'prefix' => 'oauth'], function () {
            Route::any('/activationCode/{activationCode}', ['as' => 'activation', 'uses' => 'OAuthController@activationCode']);
            Route::post('/password/forgot', ['as' => 'forgot', 'uses' => 'ReminderController@forgot']);
            Route::post('/password/reset', ['as' => 'reset', 'uses' => 'ReminderController@reset']);
            Route::post('/access_token', ['as' => 'login', 'uses' => 'OAuthController@accessToken']);
        });

        Route::post('users', ['as' => 'users.store', 'uses' => 'UserController@store']);
        Route::post('invitations/accepted/{invitationCode}', ['as' => 'accepted', 'uses' => 'InvitationController@accepted']);


        Route::group(['middleware' => ['oauth']], function () {

            Route::group(['as' => 'oauth::', 'prefix' => 'oauth'], function () {
                Route::any('/logout', ['as' => 'logout', 'uses' => 'OAuthController@expireToken']);
            });

            Route::group(['as' => 'users::', 'prefix' => 'users'], function () {
                Route::get('/', ['as' => 'index', 'uses' => 'UserController@index']);
                Route::get('/auth', ['as' => 'auth', 'uses' => 'UserController@auth']);
                Route::get('/{id}', ['as' => 'show', 'uses' => 'UserController@show']);
                Route::put('/{id}', ['as' => 'update', 'uses' => 'UserController@update']);
                Route::delete('/{id}', ['as' => 'destroy', 'uses' => 'UserController@destroy']);

                Route::resource('/profiles', 'UserProfileController');
            });

            Route::resource('sectors', 'SectorController');
            Route::resource('taxesFrameworks', 'TaxFrameworkController');
            Route::resource('providers/categories', 'ProviderCategoryController');
            Route::resource('benefits/categories', 'BenefitCategoryController');

            Route::resource('companies', 'CompanyController');
            Route::resource('medias', 'MediaController');

            Route::group(['prefix' => 'companies/{company_id}'], function () {
                Route::resource('/departments', 'DepartmentController');
                Route::resource('/positionsJobs', 'PositionJobController');
                Route::resource('/invitations', 'InvitationController');
                Route::resource('/subsidiaries', 'SubsidiaryController');
                Route::resource('/vacations', 'VacationController');
                Route::resource('/licenses', 'LicenseController');
                Route::resource('/absences', 'AbsenceController');
                Route::resource('/refunds', 'RefundController');
                Route::resource('/additionals', 'UserAdditionalController');
                Route::resource('/documents', 'DocumentController');
                Route::resource('/providers', 'ProviderController');
                Route::resource('/benefits', 'BenefitController');
                Route::resource('/hourlyWorkloads', 'HourlyWorkloadController');
                Route::resource('/remunerationsVariables', 'RemunerationVariablesController');
                Route::resource('/remunerations', 'RemunerationController');
                Route::resource('/notifications', 'NotificationController');
                Route::get('/teams', ['as' => 'teams.index', 'uses' => 'TeamController@index']);
                Route::get('/calendar', ['as' => 'calendar.index', 'uses' => 'CalendarsController@index']);
            });

        });

    });

});
