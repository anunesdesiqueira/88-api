<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Document;

/**
 * Class DocumentTransformer
 * @package namespace OitentaOito\Transformers;
 */
class DocumentTransformer extends TransformerAbstract
{

    /**
     * @param Document $model
     * @return array
     */
    public function transform(Document $model)
    {
        return [
            'id'                => (int) $model->id,
            'created_user_id'   => (int) $model->created_user_id,
            'user_id'           => (int) $model->user_id,
            'company_id'        => (int) $model->company_id,
            'name'              => $model->name,
            'folder'            => $model->folder,
            'access'            => $model->access,
            'file'              => (new MediaTransformer())->one($model->file),

            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at
        ];
    }
}
