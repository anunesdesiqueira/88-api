<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Role;

/**
 * Class RoleTransformer
 * @package OitentaOito\Transformers
 */
class RoleTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [

    ];


    /**
     * @param Role $model
     * @return array
     */
    public function transform(Role $model)
    {
        return [
            'id'            => (int) $model->id,
            'name'          => $model->name,
            'display_name'  => $model->display_name,
            'description'   => $model->description,

            'created_at'    => $model->created_at,
            'updated_at'    => $model->updated_at
        ];
    }
}
