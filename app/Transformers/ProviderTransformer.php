<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Provider;

/**
 * Class ProviderTransformer
 * @package namespace OitentaOito\Transformers;
 */
class ProviderTransformer extends TransformerAbstract
{

    /**
     * @param Provider $model
     * @return array
     */
    public function transform(Provider $model)
    {
        return [
            'id'                    => (int) $model->id,
            'company_id'            => (int) $model->company_id,
            'provider_category_id'  => (int) $model->provider_category_id,
            'name'                  => $model->name,
            'contact_name'          => $model->contact_name,
            'data_phone'            => $model->data_phone,
            'data_email'            => $model->data_email,
            'address'               => $model->address,
            'data_address'          => $model->data_address,
            'benefit'               => $model->benefit,

            'created_at'            => $model->created_at,
            'updated_at'            => $model->updated_at
        ];
    }
}
