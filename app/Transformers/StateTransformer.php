<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\State;

/**
 * Class StateTransformer
 * @package namespace OitentaOito\Transformers;
 */
class StateTransformer extends TransformerAbstract
{

    /**
     * Transform the \State entity
     * @param State|\State $model
     * @return array
     */
    public function transform(State $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'acronyms'   => $model->acronyms,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
