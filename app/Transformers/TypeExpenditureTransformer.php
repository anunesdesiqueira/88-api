<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\TypeExpenditure;

/**
 * Class TypeExpenditureTransformer
 * @package namespace OitentaOito\Transformers;
 */
class TypeExpenditureTransformer extends TransformerAbstract
{

    /**
     * @param TypeExpenditure $model
     * @return array
     */
    public function transform(TypeExpenditure $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'slug'       => $model->slug,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
