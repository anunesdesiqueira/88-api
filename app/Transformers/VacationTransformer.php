<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Vacation;

/**
 * Class VacationTransformer
 * @package namespace OitentaOito\Transformers;
 */
class VacationTransformer extends TransformerAbstract
{

    /**
     * @param Vacation $model
     * @return array
     */
    public function transform(Vacation $model)
    {
        return [
            'id'                    => (int) $model->id,
            'type'                  => $model->type,
            'created_user_id'       => (int) $model->created_user_id,
            'company_id'            => (int) $model->company_id,
            'user_id'               => ($model->user_id ? (int) $model->user_id : null),
            'department_id'         => ($model->department_id ? (int) $model->department_id : null),
            'vacation_bonus_days'   => ($model->vacation_bonus_days ? (int) $model->vacation_bonus_days : null),
            'name'                  => $model->name,
            'status'                => $model->status,
            'starting'              => ($model->starting ? $model->starting : null),
            'ending'                => ($model->ending ? $model->ending : null),
            'days'                  => ($model->days ? $model->days : null),

            'created_at'            => $model->created_at,
            'updated_at'            => $model->updated_at
        ];
    }
}
