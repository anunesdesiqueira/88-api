<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Invitation;

/**
 * Class InvitationTransformer
 * @package namespace OitentaOito\Transformers;
 */
class InvitationTransformer extends TransformerAbstract
{

    /**
     * @param Invitation $model
     * @return array
     */
    public function transform(Invitation $model)
    {
        return [
            'id'                => (int) $model->id,
            'user_id'           => ($model->user_id ? (int) $model->user_id : null ),
            'company_id'        => (int) $model->company_id,
            'department_id'     => (int) $model->department_id,
            'position_job_id'   => (int) $model->position_job_id,
            'role_id'           => (int) $model->role_id,
            'email'             => $model->email,
            'name'              => $model->name,
            'invitation_code'   => $model->invitation_code,
            'accepted'          => (int) $model->accepted,
            'accepted_at'       => $model->accepted_at,

            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at
        ];
    }
}
