<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\License;

/**
 * Class LicenseTransformer
 * @package namespace OitentaOito\Transformers;
 */
class LicenseTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'user',
    ];

    /**
     * @param License $model
     * @return array
     */
    public function transform(License $model)
    {
        return [
            'id'                => (int) $model->id,
            'created_user_id'   => (int) $model->created_user_id,
            'user_id'           => (int) $model->user_id,
            'company_id'        => (int) $model->company_id,
            'status'            => $model->status,
            'starting'          => $model->starting,
            'ending'            => $model->ending,
            'type'              => $model->type,
            'icd'               => $model->icd,
            'files'             => (new MediaTransformer())->items($model->files),

            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at
        ];
    }


    /**
     * @param License $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(License $model)
    {
        $user = $model->user;
        return $this->item($user, new UserTransformer);
    }
}
