<?php

namespace OitentaOito\Transformers;

use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Media;

/**
 * Class MediaTransformer
 * @package namespace OitentaOito\Transformers;
 */
class MediaTransformer extends TransformerAbstract
{

    /**
     * @param Media $model
     * @return array
     */
    private function getFile(Media $model)
    {
        $file = [
            "original"  => ($model->file ? $model->file->url() : null),
            "medium"    => ($model->file ? $model->file->url("medium") : null),
            "thumb"     => ($model->file ? $model->file->url("thumb") : null),
        ];

        return $file;
    }


    /**
     * @param $media
     * @return array|null
     */
    public function one($media)
    {
        if(!$media)
            return null;

        return $this->transform($media);
    }


    /**
     * @param $medias
     * @return array|Collection
     */
    public function items($medias)
    {
        if(!$medias)
            return [];

        $collection = new Collection();
        foreach($medias as $media){
            $collection->push($this->one($media));
        }

        return $collection;
    }


    /**
     * @param Media $model
     * @return array
     */
    public function transform(Media $model)
    {
        return [
            'id'                => (int) $model->id,
            'created_user_id'   => (int) $model->created_user_id,
            'user_id'           => ($model->user_id ? (int) $model->user_id : null),
            'company_id'        => ($model->company_id ? (int) $model->company_id : null),
            'reference'         => ($model->reference ? (int) $model->reference : null),
            'slug'              => $model->slug,
            'name'              => ($model->name ? $model->name : null),
            'folder'            => ($model->folder ? $model->folder : null),
            'access'            => ($model->access ? $model->access : null),
            'file_file_name'    => urldecode($model->file_file_name),
            'file'              => $this->getFile($model),
            'size'              => (float) $model->file_file_size,
            'content_type'      => $model->file_content_type,
            'file_updated_at'   => $model->file_updated_at,

            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at
        ];
    }
}
