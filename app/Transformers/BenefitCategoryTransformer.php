<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\BenefitCategory;

/**
 * Class BenefitCategoryTransformer
 * @package OitentaOito\Transformers
 */
class BenefitCategoryTransformer extends TransformerAbstract
{

    /**
     * @param BenefitCategory $model
     * @return array
     */
    public function transform(BenefitCategory $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'slug'       => $model->slug,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
