<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Calendar;

/**
 * Class CalendarTransformer
 * @package namespace OitentaOito\Transformers;
 */
class CalendarTransformer extends TransformerAbstract
{

    /**
     * @param Calendar $model
     * @return array
     */
    public function transform(Calendar $model)
    {
        return [
            'slug'              => $model->slug,
            'company_id'        => $model->company_id,
            'name'              => $model->name,
            'starting_date'     => $model->starting_date,
            'ending_date'       => $model->ending_date,
        ];
    }
}
