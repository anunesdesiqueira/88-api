<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\UserAdditional;

/**
 * Class UserAdditionalTransformer
 * @package namespace OitentaOito\Transformers;
 */
class UserAdditionalTransformer extends TransformerAbstract
{

    /**
     * @param UserAdditional $model
     * @return array
     */
    public function transform(UserAdditional $model)
    {
        return [
            'id'                    => (int) $model->id,
            'user_id'               => (int) $model->user_id,
            'company_id'            => (int) $model->company_id,
            'department_id'         => ($model->department_id ? (int) $model->department_id : null),
            'department'            => ($model->department ? $model->department->name : null),
            'position_job_id'       => ($model->position_job_id ? (int) $model->position_job_id : null),
            'position_job'          => ($model->positionJob ? $model->positionJob->name : null),
            'education_level_id'    => ($model->education_level_id ? (int) $model->education_level_id : null),
            'education_level'       => ($model->educationLevel ? $model->educationLevel->name : null),
            'admission_date'        => $model->admission_date,
            'base_salary'           => $model->base_salary,
            'payday'                => $model->payday,
            'bank_data'             => $model->bank_data,
            'special_needs'         => $model->special_needs,
            'discounts'             => $model->discounts,

            'created_at'            => $model->created_at,
            'updated_at'            => $model->updated_at
        ];
    }
}
