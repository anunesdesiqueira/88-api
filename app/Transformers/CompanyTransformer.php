<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Company;

/**
 * Class CompanyTransformer
 * @package namespace OitentaOito\Transformers;
 */
class CompanyTransformer extends TransformerAbstract
{

    /**
     * @param Company $model
     * @return array
     */
    public function transform(Company $model)
    {
        return [
            'id'                                    => (int) $model->id,
            'type'                                  => $model->type,
            'slug'                                  => $model->slug,
            'trading_name'                          => $model->trading_name,
            'company_name'                          => $model->company_name,
            'national_register_of_legal_entities'   => $model->national_register_of_legal_entities,
            'state_registration'                    => $model->state_registration,
            'sector_id'                             => $model->sector_id,
            'sector_name'                           => $model->sector_name,
            'tax_framework_id'                      => $model->tax_framework_id,
            'address'                               => $model->address,
            'data_address'                          => $model->data_address,
            'data_email'                            => $model->data_email,
            'data_phone'                            => $model->data_phone,
            'avatar'                                => (new MediaTransformer())->one($model->avatar),

            'created_at'                            => $model->created_at,
            'updated_at'                            => $model->updated_at
        ];
    }
}
