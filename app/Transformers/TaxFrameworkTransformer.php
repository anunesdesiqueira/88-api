<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\TaxFramework;

/**
 * Class SectorTransformer
 * @package namespace OitentaOito\Transformers;
 */
class TaxFrameworkTransformer extends TransformerAbstract
{

    /**
     * @param TaxFramework $model
     * @return array
     */
    public function transform(TaxFramework $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'slug'       => $model->slug,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
