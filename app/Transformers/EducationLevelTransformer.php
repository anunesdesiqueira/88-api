<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\EducationLevel;

/**
 * Class EducationLevelTransformer
 * @package OitentaOito\Transformers
 */
class EducationLevelTransformer extends TransformerAbstract
{

    /**
     * @param EducationLevel $model
     * @return array
     */
    public function transform(EducationLevel $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'slug'       => $model->slug,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
