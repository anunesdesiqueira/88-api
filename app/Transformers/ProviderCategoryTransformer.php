<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\ProviderCategory;

/**
 * Class ProviderCategoryTransformer
 * @package namespace OitentaOito\Transformers;
 */
class ProviderCategoryTransformer extends TransformerAbstract
{

    /**
     * @param ProviderCategory $model
     * @return array
     */
    public function transform(ProviderCategory $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'slug'       => $model->slug,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
