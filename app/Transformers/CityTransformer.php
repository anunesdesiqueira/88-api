<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\City;

/**
 * Class CityTransformer
 * @package namespace OitentaOito\Transformers;
 */
class CityTransformer extends TransformerAbstract
{

    /**
     * Transform the \City entity
     * @param \City|City $model
     * @return array
     */
    public function transform(City $model)
    {
        return [
            'id'         => (int) $model->id,
            'state_id'   => (int) $model->state_id,
            'name'       => $model->name,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
