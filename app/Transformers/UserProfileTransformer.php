<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\UserProfile;

/**
 * Class UserProfileTransformer
 * @package namespace OitentaOito\Transformers;
 */
class UserProfileTransformer extends TransformerAbstract
{

    /**
     * Transform the \UserProfile entity
     * @param UserProfile|\UserProfile $model
     * @return array
     */
    public function transform(UserProfile $model)
    {
        return [
            'id'                                    => (int) $model->id,
            'user_id'                               => (int) $model->user_id,
            'name'                                  => $model->name,
            'gender'                                => $model->gender,
            'date_of_birth'                         => $model->date_of_birth,
            'identity_document'                     => $model->identity_document,
            'identity_document_state'               => $model->identity_document_state,
            'identity_document_dispatcher_organ'    => $model->identity_document_dispatcher_organ,
            'identity_document_dispatcher_date'     => $model->identity_document_dispatcher_date,
            'register_individual'                   => $model->register_individual,
            'voter_id_card'                         => $model->voter_id_card,
            'driving_licence'                       => $model->driving_licence,
            'driving_licence_category'              => $model->driving_licence_category,
            'driving_licence_dispatcher_date'       => $model->driving_licence_dispatcher_date,
            'driving_licence_validate_date'         => $model->driving_licence_validate_date,
            'work_record_booklet'                   => $model->work_record_booklet,
            'work_record_booklet_series'            => $model->work_record_booklet_series,
            'work_record_booklet_dispatcher_state'  => $model->work_record_booklet_dispatcher_state,
            'social_integration_program'            => $model->social_integration_program,
            'professional_identity'                 => $model->professional_identity,
            'address'                               => $model->address,
            'data_address'                          => $model->data_address,
            'data_email'                            => $model->data_email,
            'data_phone'                            => $model->data_phone,
            'data_phone_emergency'                  => $model->data_phone_emergency,
            'about_me'                              => $model->about_me,
            'created_at'                            => $model->created_at,
            'updated_at'                            => $model->updated_at
        ];
    }
}
