<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Company;

/**
 * Class SubsidiaryTransformer
 * @package namespace OitentaOito\Transformers;
 */
class SubsidiaryTransformer extends TransformerAbstract
{

    /**
     * @param Company $model
     * @return array
     */
    public function transform(Company $model)
    {
        return [
            'id'                                    => (int) $model->id,
            'type'                                  => $model->type,
            'trading_name'                          => $model->trading_name,
            'national_register_of_legal_entities'   => $model->national_register_of_legal_entities,
            'address'                               => $model->address,
            'data_address'                          => $model->data_address,
            'data_email'                            => $model->data_email,
            'data_phone'                            => $model->data_phone,

            'created_at'                            => $model->created_at,
            'updated_at'                            => $model->updated_at
        ];
    }
}
