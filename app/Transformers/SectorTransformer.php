<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Sector;

/**
 * Class SectorTransformer
 * @package namespace OitentaOito\Transformers;
 */
class SectorTransformer extends TransformerAbstract
{

    /**
     * Transform the \Sector entity
     * @param Sector|\Sector $model
     * @return array
     */
    public function transform(Sector $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'slug'       => $model->slug,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
