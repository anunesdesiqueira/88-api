<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Absence;

/**
 * Class AbsenceTransformer
 * @package namespace OitentaOito\Transformers;
 */
class AbsenceTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'user',
    ];


    /**
     * @param Absence $model
     * @return array
     */
    public function transform(Absence $model)
    {
        return [
            'id'                => (int) $model->id,
            'created_user_id'   => (int) $model->created_user_id,
            'user_id'           => (int) $model->user_id,
            'company_id'        => (int) $model->company_id,
            'status'            => $model->status,
            'starting'          => $model->starting,
            'ending'            => $model->ending,
            'comment'           => $model->comment,
            'justified'         => $model->justified,
            'file'              => (new MediaTransformer())->one($model->file),

            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at
        ];
    }


    /**
     * @param Absence $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(Absence $model)
    {
        $user = $model->user;
        return $this->item($user, new UserTransformer);
    }
}
