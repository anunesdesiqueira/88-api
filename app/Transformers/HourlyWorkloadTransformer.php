<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\HourlyWorkload;

/**
 * Class HourlyWorkloadTransformer
 * @package namespace OitentaOito\Transformers;
 */
class HourlyWorkloadTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'users',
    ];


    /**
     * @param HourlyWorkload $model
     * @return array
     */
    public function transform(HourlyWorkload $model)
    {
        return [
            'id'                => (int) $model->id,
            'created_user_id'   => (int) $model->created_user_id,
            'company_id'        => (int) $model->company_id,
            'name'              => $model->name,
            'data_week'         => $model->data_week,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }


    /**
     * @param HourlyWorkload $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeUsers(HourlyWorkload $model)
    {
        $users = $model->users;

        if(!is_null($users))
            return $this->collection($users, new UserTransformer);
    }
}
