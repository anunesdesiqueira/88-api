<?php

namespace OitentaOito\Transformers;

use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\User;

/**
 * Class UserTransformer
 * @package namespace OitentaOito\Transformers;
 */
class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'additional',
        'roleUser',

    ];


    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'profile',
    ];

    /**
     * @var array
     */
    private $validParams = ['company'];


    /**
     * Transform the \User entity
     * @param User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        return [
            'id'            => (int) $model->id,
            'email'         => $model->email,
            'display_name'  => ($model->profile ? $model->profile->name : $model->email),
            'avatar'        => (new MediaTransformer())->one($model->avatar),

            'created_at'    => $model->created_at,
            'updated_at'    => $model->updated_at
        ];
    }


    /**
     * @param User $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeProfile(User $model)
    {
        $profile = $model->profile;

        if(!is_null($profile))
            return $this->item($profile, new UserProfileTransformer);
    }


    /**
     * @param User $model
     * @param ParamBag|null $params
     * @return \League\Fractal\Resource\Item
     * @throws \Exception
     */
    public function includeAdditional(User $model, ParamBag $params = null)
    {
        $additional = $model->additional();

        if(!is_null($params)) {
            $usedParams = array_keys(iterator_to_array($params));
            if ($invalidParams = array_diff($usedParams, $this->validParams)) {
                throw new \Exception(sprintf(
                    'Invalid param(s): "%s". Valid param(s): "%s"',
                    implode(',', $usedParams),
                    implode(',', $this->validParams)
                ));
            }

            list($companyId) = $params->get('company');
            $additional->where('company_id', $companyId);
        }

        $additional = $additional->first();

        if(!is_null($additional))
            return $this->item($additional, new UserAdditionalTransformer);
    }


    /**
     * @param User $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeRoleUser(User $model)
    {
        $roleUser = $model->roleUser;

        if(!is_null($roleUser))
            return $this->collection($roleUser, new RoleUserTransformer);
    }
}
