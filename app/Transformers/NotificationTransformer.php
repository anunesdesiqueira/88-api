<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Notification;

/**
 * Class NotificationTransformer
 * @package namespace OitentaOito\Transformers;
 */
class NotificationTransformer extends TransformerAbstract
{



    /**
     * @param Notification $model
     * @return array
     */
    public function transform(Notification $model)
    {
        return [
            'id'                => (int) $model->id,
            'user_id'           => (int) $model->user_id,
            'company_id'        => (int) $model->company_id,
            'display_name'      => ($model->user->profile ? $model->user->profile->name : $model->user->email),
            'avatar'            => (new MediaTransformer())->one($model->user->avatar),
            'notification_at'   => $model->notification_at,
            'slug'              => $model->slug,
            'message'           => $model->message,

            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at
        ];
    }
}
