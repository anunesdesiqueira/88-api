<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\RoleUser;

/**
 * Class RoleTransformer
 * @package OitentaOito\Transformers
 */
class RoleUserTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'company'
    ];
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'role'
    ];


    /**
     * @param RoleUser $model
     * @return array
     */
    public function transform(RoleUser $model)
    {
        return [
            'user_id'       => (int) $model->user_id,
            'company_id'    => (int) $model->company_id,
            'role_id'       => (int) $model->role_id,

            'created_at'    => $model->created_at,
            'updated_at'    => $model->updated_at
        ];
    }


    /**
     * @param RoleUser $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeRole(RoleUser $model)
    {
        $role = $model->role;

        if(!is_null($role))
            return $this->item($role, new RoleTransformer);
    }


    /**
     * @param RoleUser $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeCompany(RoleUser $model)
    {
        $company = $model->company;

        if(!is_null($company))
            return $this->item($company, new CompanyTransformer);
    }
}
