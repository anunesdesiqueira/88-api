<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Benefit;

/**
 * Class BenefitTransformer
 * @package namespace OitentaOito\Transformers;
 */
class BenefitTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'beneficiaries',
    ];


    /**
     * @param Benefit $model
     * @return array
     */
    public function transform(Benefit $model)
    {
        return [
            'id'                    => (int) $model->id,
            'provider_id'           => (int) $model->provider_id,
            'company_id'            => (int) $model->company_id,
            'benefit_category_id'   => (int) $model->benefit_category_id,
            'name'                  => $model->name,
            'description'           => $model->description,

            'created_at'            => $model->created_at,
            'updated_at'            => $model->updated_at,
        ];
    }


    /**
     * @param Benefit $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeBeneficiaries(Benefit $model)
    {
        $beneficiaries = $model->beneficiaries;

        if(!is_null($beneficiaries))
            return $this->collection($beneficiaries, new UserTransformer);
    }
}
