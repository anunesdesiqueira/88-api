<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Refund;

/**
 * Class RefundTransformer
 * @package namespace OitentaOito\Transformers;
 */
class RefundTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'user',
    ];


    /**
     * @param Refund $model
     * @return array
     */
    public function transform(Refund $model)
    {
        return [
            'id'                    => (int) $model->id,
            'created_user_id'       => (int) $model->created_user_id,
            'user_id'               => (int) $model->user_id,
            'company_id'            => (int) $model->company_id,
            'type_expenditure_id'   => (int) $model->type_expenditure_id,
            'status'                => $model->status,
            'date'                  => $model->date,
            'value'                 => (float) $model->value,
            'amount_approved'       => (float) $model->amount_approved,
            'description'           => $model->description,
            'justification'         => $model->justification,
            'file_returnable'       => (new MediaTransformer())->items($model->fileReturnable),
            'file_receipt'          => (new MediaTransformer())->items($model->fileReceipt),

            'created_at'            => $model->created_at,
            'updated_at'            => $model->updated_at
        ];
    }


    /**
     * @param Refund $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(Refund $model)
    {
        $user = $model->user;
        return $this->item($user, new UserTransformer);
    }
}
