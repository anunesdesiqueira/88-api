<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\Remuneration;

/**
 * Class RemunerationTransformer
 * @package namespace OitentaOito\Transformers;
 */
class RemunerationTransformer extends TransformerAbstract
{

    /**
     * @param Remuneration $model
     * @return array
     */
    public function transform(Remuneration $model)
    {
        return [
            'id'            => (int) $model->id,
            'user_id'       => (int) $model->user_id,
            'company_id'    => (int) $model->company_id,
            'name'          => $model->name,
            'payday'        => $model->payday,
            'base_salary'   => $model->base_salary,
            'benefits'      => $model->benefits,
            'discounts'     => $model->discounts,
            'net_salary'    => $model->net_salary,
            'variables'     => $model->variables,
            'payslip'       => (new MediaTransformer())->one($model->payslip),

            'created_at'    => $model->created_at,
            'updated_at'    => $model->updated_at
        ];
    }
}
