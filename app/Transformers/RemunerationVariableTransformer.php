<?php

namespace OitentaOito\Transformers;

use League\Fractal\TransformerAbstract;
use OitentaOito\Entities\RemunerationVariable;

/**
 * Class RemunerationVariableTransformer
 * @package namespace OitentaOito\Transformers;
 */
class RemunerationVariableTransformer extends TransformerAbstract
{

    /**
     * @param RemunerationVariable $model
     * @return array
     */
    public function transform(RemunerationVariable $model)
    {
        return [
            'id'         => (int) $model->id,
            'company_id' => ($model->company_id ? (int) $model->company_id : null),
            'name'       => $model->name,
            'slug'       => $model->slug,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
