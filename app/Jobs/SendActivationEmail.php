<?php

namespace OitentaOito\Jobs;

use OitentaOito\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use OitentaOito\Entities\User;
use Illuminate\Contracts\Mail\Mailer;

class SendActivationEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var User
     */
    protected $user;

    /**
     * Create a new job instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @param Mailer $mailer
     */
    public function handle(Mailer $mailer)
    {
        $data = [
            'email' => $this->user->email,
            'activationCode' => $this->user->activation_code,
        ];

        $mailer->send('emails.auth.activation', $data, function ($message) use ($data) {
            $message->to($data['email'])->subject('Email de Ativação - Oitenta e Oito');
        });
    }

}
