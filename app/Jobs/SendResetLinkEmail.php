<?php

namespace OitentaOito\Jobs;

use OitentaOito\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Auth\PasswordBroker as PasswordBrokerContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class SendResetLinkEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;


    /**
     * @var CanResetPasswordContract
     */
    protected $user;


    /**
     * SendResetLinkEmail constructor.
     * @param CanResetPasswordContract $user
     */
    public function __construct(CanResetPasswordContract $user)
    {
        $this->user = $user;
    }


    /**
     * @param Mailer $mailer
     * @param PasswordBrokerContract $passwordBroker
     */
    public function handle(Mailer $mailer, PasswordBrokerContract $passwordBroker)
    {
        try {

            $user = $this->user;
            $token = $passwordBroker->createToken($user);
//            $profile = $this->user->profile()->first();

            $mailer->send('emails.auth.password', compact('token'), function ($message) use ($user) {
                $message->to($user->getEmailForPasswordReset())->subject('Esqueci minha senha - Oitenta e Oito');
            });


        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
