<?php

namespace OitentaOito\Jobs;

use OitentaOito\Entities\Invitation;
use OitentaOito\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendInvitationMail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var Invitation
     */
    protected $invitation;


    /**
     * SendInvitationMail constructor.
     * @param Invitation $invitation
     */
    public function __construct(Invitation $invitation)
    {
        $this->invitation = $invitation;
    }


    /**
     * Execute the job.
     *
     * @param Mailer $mailer
     */
    public function handle(Mailer $mailer)
    {
        $data = [
            'email' => $this->invitation->email,
            'name' => $this->invitation->name,
            'invitationCode' => $this->invitation->invitation_code,
            'tradingName' => $this->invitation->company->trading_name,
            'frontUrl' => env('FRONT_URL'),
        ];

        $mailer->send('emails.invitations.invitation', $data, function ($message) use ($data) {
            $message->to($data['email'])->subject('Convite - Oitenta e Oito');
        });
    }
}
