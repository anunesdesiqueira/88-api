<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Validators\ProviderValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use OitentaOito\Repositories\ProviderRepository;

class ProviderService
{
    /**
     * @var ProviderRepository
     */
    protected $repository;


    /**
     * @var ProviderValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * @param ProviderRepository $repository
     * @param ProviderValidator $validator
     * @param ResponseService $response
     */
    public function __construct(ProviderRepository $repository, ProviderValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param $companyId
     * @param $limit
     * @return Response
     */
    public function all($companyId, $limit)
    {
        $positionsJobs = $this->repository->scopeQuery(function($query) use ($companyId) {
            return $query->where('company_id', $companyId);
        })
            ->orderBy('name', 'asc')
            ->paginate($limit);

        return $this->response->setData($positionsJobs)->respondWithSuccess();
    }


    /**
     * @param $companyId
     * @param array $data
     * @return Response
     */
    public function create($companyId, array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $providers = $this->repository->create([
                'created_user_id' => Authorizer::getResourceOwnerId(),
                'company_id' => $companyId,
                'provider_category_id' => $data['provider_category_id'],
                'name' => $data['name'],
                'contact_name' => $data['contact_name'],
                'data_phone' => $data['data_phone'],
                'data_email' => $data['data_email'],
                'address' => $data['address'],
                'data_address' => $data['data_address'],
                'benefit' => $data['benefit']
            ]);

            return $this->response->setData($providers)
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }

    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function find($companyId, $id)
    {
        try {

            $provider = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->find($id);

            return $this->response->setData($provider)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @param array $data
     * @return Response
     */
    public function update($companyId, $id, array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $provider = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->update([
                'provider_category_id' => $data['provider_category_id'],
                'name' => $data['name'],
                'contact_name' => $data['contact_name'],
                'data_phone' => $data['data_phone'],
                'data_email' => $data['data_email'],
                'address' => $data['address'],
                'data_address' => $data['data_address'],
                'benefit' => $data['benefit']
            ], $id);

            return $this->response->setData($provider)
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function delete($companyId, $id)
    {
        try {

            $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }
}