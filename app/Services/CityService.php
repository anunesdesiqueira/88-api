<?php

namespace OitentaOito\Services;

use OitentaOito\Repositories\CityRepository;

class CityService
{
    /**
     * @var CityRepository
     */
    protected $repository;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * CityService constructor.
     * @param CityRepository $repository
     * @param ResponseService $response
     */
    public function __construct(CityRepository $repository, ResponseService $response)
    {
        $this->repository = $repository;
        $this->response = $response;
    }


    /**
     * @param array $data
     * @return Response
     */
    public function all(array $data)
    {
        $cities = $this->repository->orderBy("name","asc")->all();
        return $this->response->setData($cities)->respondWithSuccess();
    }
}