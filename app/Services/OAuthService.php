<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Repositories\UserRepository;

class OAuthService {

    /**
     * @var UserRepository
     */
    protected $repository;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * UserService constructor.
     * @param \OitentaOito\Repositories\UserRepository $repository
     * @param \OitentaOito\Services\ResponseService $response
     */
    public function __construct(UserRepository $repository, ResponseService $response)
    {
        $this->repository = $repository;
        $this->response = $response;
    }


    /**
     * @param $activationCode
     * @return Response
     */
    public function activationCode($activationCode)
    {
        try {

            $userNotActivation = $this->repository->skipPresenter()
                ->findByField('activation_code', $activationCode)->first();
            $user = $this->repository->skipPresenter(0)
                ->update(['activation_code' => null, 'activated' => 1], $userNotActivation['id']);

            return $this->response->setData($user)
                ->setMessage(trans("messages.success.activation_success"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @return Response
     */
    public function accessToken()
    {
        $authorize = Authorizer::issueAccessToken();
        return $this->response->setData($authorize)->respondWithSuccess();
    }


    /**
     * @return Response
     */
    public function expireToken()
    {
        Authorizer::getChecker()->getAccessToken()->expire();
        Auth::logout();

        return $this->response->respondWithSuccess();
    }

}