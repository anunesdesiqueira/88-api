<?php

namespace OitentaOito\Services;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use OitentaOito\Repositories\CalendarRepository;
use OitentaOito\Validators\CalendarValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class CalendarService
{
    /**
     * @var CalendarRepository
     */
    protected $repository;


    /**
     * @var CalendarValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * CalendarService constructor.
     * @param CalendarRepository $repository
     * @param CalendarValidator $validator
     * @param ResponseService $response
     */
    public function __construct(CalendarRepository $repository, CalendarValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param array $data
     * @return array
     */
    protected function getFromDateToDate(array $data)
    {
        if(isset($data['fromDate'])) {
            $fromDate = Carbon::parse($data['fromDate']);
        } else {
            $fromDate = Carbon::now()->startOfMonth();
        }

        if(isset($data['toDate'])) {
            $toDate = Carbon::parse($data['toDate']);
        } else {
            $toDate = Carbon::now()->endOfMonth();
        }

        return [$fromDate, $toDate];
    }


    /**
     * @param $companyId
     * @param array $data
     * @return Response
     */
    public function all($companyId, array $data)
    {
        $dates = $this->getFromDateToDate($data);
        $calendar = $this->repository->scopeQuery(function ($query) use ($dates, $companyId) {
            return $query->where('starting_date', '>=', $dates[0]->toDateString())->where('starting_date', '<=', $dates[1]->toDateString())
                    ->orWhere(function ($query) use ($dates) {
                        $query->where('slug', 'birthday_date')
                              ->where(\DB::raw("CONVERT(SUBSTRING_INDEX(starting_date, '-', 1),UNSIGNED INTEGER)"), '>=', $dates[0]->month)
                              ->where(\DB::raw("CONVERT(SUBSTRING_INDEX(starting_date, '-', 1),UNSIGNED INTEGER)"), '<=', $dates[1]->month)
                              ->where(\DB::raw("CONVERT(SUBSTRING_INDEX(starting_date, '-', -1),UNSIGNED INTEGER)"), '>=', $dates[0]->day)
                              ->where(\DB::raw("CONVERT(SUBSTRING_INDEX(starting_date, '-', -1),UNSIGNED INTEGER)"), '<=', $dates[1]->day);
                    })
                    ->where('company_id', (int) $companyId)
                    ->orWhere(function ($query) {
                        $query->where('company_id', null);
                    });

        })->all();

        return $this->response->setData($calendar)->respondWithSuccess();
    }
}