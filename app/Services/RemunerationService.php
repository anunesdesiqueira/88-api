<?php

namespace OitentaOito\Services;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Criteria\RemunerationCriteria;
use OitentaOito\Entities\Media;
use OitentaOito\Entities\User;
use OitentaOito\Validators\RemunerationValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use OitentaOito\Repositories\RemunerationRepository;

class RemunerationService
{
    /**
     * @var RemunerationRepository
     */
    protected $repository;


    /**
     * @var RemunerationValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * RemunerationService constructor.
     * @param RemunerationRepository $repository
     * @param RemunerationValidator $validator
     * @param ResponseService $response
     */
    public function __construct(RemunerationRepository $repository, RemunerationValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    protected function getUserRemuneration($companyId, $userId)
    {
        $user = User::find($userId);
        $profile = $user->profile;
        $additional = $user->additional->where('company_id', $companyId)->first();
        $paydays = $additional->payday;
        $baseSalary = $additional->base_salary;
        $netSalary = $baseSalary;
        $discounts = $additional->discounts;

        $now = Carbon::now();
        $first = Carbon::now()->day($paydays[0]);
        $payday = $first;

        if(count($paydays) > 1) {
            $second = Carbon::now()->day($paydays[1]);

            if($now > $first) {
                $payday = $second;
            }
        }

        $benefits = 0;
        foreach ($discounts as $discount) {
            $pivot = $discount->pivot;

            if("percentage" === $pivot->type) {
                $benefits += ($pivot->value / 100) * $baseSalary;
            } else {
                $benefits += $pivot->value;
            }
        }

        $discounts = $benefits;
        $netSalary -= $discounts;

        return [
            'created_user_id' => Authorizer::getResourceOwnerId(),
            'user_id' => $userId,
            'company_id' => $companyId,
            'name' => $profile->name,
            'payday' => $payday,
            'base_salary' => $baseSalary,
            'benefits' => $benefits,
            'discounts' => $discounts,
            'net_salary' => $netSalary,
        ];
    }


    /**
     * @param $companyId
     * @param array $data
     * @return Response
     */
    public function all($companyId, array $data)
    {
        $remunerations = $this->repository->scopeQuery(function($query) use ($companyId, $data) {
            $query->where('company_id', $companyId);

            if($data['fromDate'])
                $query->where('payday', '>=', $data['fromDate']);

            if($data['toDate'])
                $query->where('payday', '<=', $data['toDate']);

            return $query;
        })
            ->orderBy('name', 'asc')
            ->paginate($data['limit']);

        return $this->response->setData($remunerations)->respondWithSuccess();
    }


    /**
     * @param $companyId
     * @param array $data
     * @return Response
     */
    public function create($companyId, array $data)
    {
        try {

            $data = $this->getUserRemuneration($companyId, $data['user_id']);
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $remuneration = $this->repository->create($data);

            return $this->response->setData($remuneration)
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function find($companyId, $id)
    {
        try {

            $remuneration = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId)
                    ->orWhere('company_id', null);
            })
                ->find($id);

            return $this->response->setData($remuneration)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @param array $data
     * @return Response
     */
    public function update($companyId, $id, array $data)
    {
        try {

            $this->validator->setId($id);
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $remuneration = $this->repository->skipPresenter(1)->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->update($data, $id);

            if(isset($data['variable']) && $data['variable'])
                $remuneration->variables()->attach($data['variable']['remuneration_variable_id'], ['value_or_amount' => $data['variable']['value_or_amount']]);

            if(isset($data['file_id']) && $data['file_id'])
                Media::setMyReference($data['file_id'], $remuneration->id);

            return $this->response->setData($this->repository->skipPresenter(0)->find($remuneration->id))
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function delete($companyId, $id)
    {
        try {

            $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })
                ->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }
}