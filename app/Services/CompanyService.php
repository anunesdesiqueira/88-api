<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Entities\Media;
use OitentaOito\Validators\CompanyValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use OitentaOito\Repositories\CompanyRepository;

class CompanyService
{
    /**
     * @var CompanyRepository
     */
    protected $repository;


    /**
     * @var CompanyValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * @var
     */
    protected $user;


    /**
     * @param CompanyRepository $repository
     * @param CompanyValidator $validator
     * @param ResponseService $response
     */
    public function __construct(CompanyRepository $repository, CompanyValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
        $this->user = Authorizer::getResourceOwnerId();
    }


    /**
     * @return Response
     */
    public function all()
    {

        $companies = $this->repository->whereHas('users', function ($query) {
            $query->where('user_id', $this->user);
        })
            ->orderBy("trading_name","asc")
            ->all();

        return $this->response->setData($companies)->respondWithSuccess();
    }


    /**
     * @param array $data
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function create(array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $company = $this->repository->skipPresenter(1)->create([
                'type' => 'headquarters',
                'slug' => $data['company_name'],
                'trading_name' => $data['trading_name'],
                'company_name' => $data['company_name'],
                'national_register_of_legal_entities' => $data['national_register_of_legal_entities'],
                'state_registration' => $data['state_registration'],
                'sector_id' => $data['sector_id'],
                'sector_name' => $data['sector_name'],
                'tax_framework_id' => $data['tax_framework_id'],
                'address' => $data['address'],
                'data_address' => $data['data_address'],
                'data_email' => $data['data_email'],
                'data_phone' => $data['data_phone'],
            ]);

            $company->users()->attach($this->user, ['role_id' => 1]);

            if(isset($data['file_id']) && $data['file_id'])
                Media::setMyReference($data['file_id'], $company->id);

            return $this->response->setData($this->repository->skipPresenter(0)->find($company->id))
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $id
     * @return Response
     */
    public function find($id)
    {
        try {

            $company = $this->repository->whereHas('users', function ($query) {
                $query->where('user_id', $this->user);
            })
                ->find($id);

            return $this->response->setData($company)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param array $data
     * @param $id
     * @return Response
     */
    public function update(array $data, $id)
    {
        try {
            $this->validator->setId($id);
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $company = $this->repository->skipPresenter(1)->whereHas('users', function ($query) {
                $query->where('user_id', $this->user);
            })->update([
                'slug' => $data['company_name'],
                'trading_name' => $data['trading_name'],
                'company_name' => $data['company_name'],
                'national_register_of_legal_entities' => $data['national_register_of_legal_entities'],
                'state_registration' => $data['state_registration'],
                'sector_id' => $data['sector_id'],
                'sector_name' => $data['sector_name'],
                'tax_framework_id' => $data['tax_framework_id'],
                'address' => $data['address'],
                'data_address' => $data['data_address'],
                'data_email' => $data['data_email'],
                'data_phone' => $data['data_phone'],
            ], $id);

            if(isset($data['file_id']) && $data['file_id'])
                Media::setMyReference($data['file_id'], $company->id);

            return $this->response->setData($this->repository->skipPresenter(0)->find($company->id))
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $id
     * @return Response
     */
    public function delete($id)
    {
        try {

            $this->repository->whereHas('users', function ($query) {
                $query->where('user_id', $this->user);
            })
                ->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }

}