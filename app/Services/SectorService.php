<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use OitentaOito\Validators\SectorValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use OitentaOito\Repositories\SectorRepository;

class SectorService
{
    /**
     * @var SectorRepository
     */
    protected $repository;


    /**
     * @var SectorValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * @param SectorRepository $repository
     * @param SectorValidator $validator
     * @param ResponseService $response
     */
    public function __construct(SectorRepository $repository, SectorValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param $limit
     * @return Response
     */
    public function all($limit)
    {
        $sectors = $this->repository->paginate($limit);
        return $this->response->setData($sectors)->respondWithSuccess();
    }


    /**
     * @param array $data
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function create(array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $sector = $this->repository->create([
                'name' => $data['name'],
                'slug' => $data['name'],
            ]);

            return $this->response->setData($sector)
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $id
     * @return Response
     */
    public function find($id)
    {
        try {

            $sector = $this->repository->find($id);
            return $this->response->setData($sector)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param array $data
     * @param $id
     * @return Response
     */
    public function update(array $data, $id)
    {
        try {
            $this->validator->setId($id);
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $sector = $this->repository->update([
                'name' => $data['name'],
                'slug' => $data['name'],
            ], $id);

            return $this->response->setData($sector)
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $id
     * @return Response
     */
    public function delete($id)
    {
        try {

            $this->repository->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }

}