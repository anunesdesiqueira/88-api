<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Entities\Media;
use OitentaOito\Validators\UserValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use OitentaOito\Repositories\UserRepository;
use OitentaOito\Jobs\SendActivationEmail;

class UserService
{
    /**
     * @var UserRepository
     */
    protected $repository;


    /**
     * @var UserValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * @param UserRepository $repository
     * @param UserValidator $validator
     * @param ResponseService $response
     */
    public function __construct(UserRepository $repository, UserValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param $user
     */
    private function SendActivationEmail($user)
    {
        dispatch((
            new SendActivationEmail($user)
        ));
    }

    /**
     * @return Response
     */
    public function findAuth()
    {
        return $this->find(Authorizer::getResourceOwnerId());
    }


    /**
     * @param $limit
     * @return Response
     */
    public function all($limit)
    {
        $users = $this->repository->paginate($limit);
        return $this->response->setData($users)->respondWithSuccess();
    }


    /**
     * @param array $data
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function create(array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $user = $this->repository->skipPresenter(1)->create([
                'email' => $data['email'],
                'password' => (isset($data['password']) && $data['password'] ? Hash::make($data['password']) : null),
                'terms_of_use' => (int) $data['terms_of_use'],
                'activation_code' => str_random(60),
            ]);

            if(isset($data['file_id']) && $data['file_id'])
                Media::setMyReference($data['file_id'], $user->id);

            $this->SendActivationEmail($user);

            return $this->response->setData($this->repository->skipPresenter(0)->find($user->id))
                ->setMessage(trans("messages.success.user_created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $id
     * @return Response
     */
    public function find($id)
    {
        try {

            $user = $this->repository->find($id);
            return $this->response->setData($user)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param array $data
     * @param $id
     * @return Response
     */
    public function update(array $data, $id)
    {
        try {

            $this->validator->setId($id);
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $user = $this->repository->skipPresenter(1)->update([
                'email' => $data['email'],
            ], $id);

            if(isset($data['file_id']) && $data['file_id'])
                Media::setMyReference($data['file_id'], $user->id);

            return $this->response->setData($this->repository->skipPresenter(0)->find($user->id))
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $id
     * @return Response
     */
    public function delete($id)
    {
        try {

            $this->repository->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }

}