<?php

namespace OitentaOito\Services;

use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Repositories\UserRepository;
use OitentaOito\Services\ResponseService;

class GoogleOAuthService
{
    /**
     * @var UserRepository
     */
    protected $repository;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * GoogleOAuthService constructor.
     * @param UserRepository $repository
     * @param \OitentaOito\Services\ResponseService $response
     */
    public function __construct(UserRepository $repository, ResponseService $response)
    {
        $this->repository = $repository;
        $this->response = $response;
    }


    /**
     * @param $socialite
     * @return mixed
     */
    protected function createOrUpdate($socialite)
    {
        $user =  $this->repository->skipPresenter()->updateOrCreate([
            'email' => $socialite->getEmail(),
            'terms_of_use' => 1,
            'activated' => 1,
        ], [
            'email' => $socialite->getEmail()
        ]);

        return $user->id;
    }


    /**
     * @return mixed
     */
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }


    /**
     *
     */
    public function callback()
    {
        $socialite = Socialite::with('google')->stateless()->user();
        dd($socialite);
    }
}