<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Entities\Media;
use OitentaOito\Traits\NotificationTrait;
use OitentaOito\Validators\LicenseValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use OitentaOito\Repositories\LicenseRepository;

class LicenseService
{
    use NotificationTrait;


    /**
     * @var LicenseRepository
     */
    protected $repository;


    /**
     * @var LicenseValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * LicenseService constructor.
     * @param LicenseRepository $repository
     * @param LicenseValidator $validator
     * @param ResponseService $response
     */
    public function __construct(LicenseRepository $repository, LicenseValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param $license
     * @param $rule
     */
    protected function notificationLicense($license, $rule)
    {
        $this->notification($license->user_id, $license->company_id, "notification_license_{$rule}", "notifications.license.{$rule}");
    }


    /**
     * @param $companyId
     * @param $limit
     * @return Response
     */
    public function all($companyId, $limit)
    {

        $licenses = $this->repository->scopeQuery(function($query) use ($companyId) {
            return $query->where('company_id', $companyId);
        })
            ->orderBy('created_at', 'desc')
            ->paginate($limit);

        return $this->response->setData($licenses)->respondWithSuccess();
    }


    /**
     * @param $companyId
     * @param array $data
     * @return Response
     */
    public function create($companyId, array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $license = $this->repository->skipPresenter(1)->create([
                'created_user_id' => Authorizer::getResourceOwnerId(),
                'company_id' => $companyId,
                'user_id' => $data['user_id'],
                'status' => $data['status'],
                'starting' => $data['starting'],
                'ending' => $data['ending'],
                'type' => $data['type'],
                'icd' => $data['icd'],
            ]);

            if(isset($data['files_id']) && $data['files_id'])
                Media::setMyReference($data['files_id'], $license->id);

            $this->notificationLicense($license, 'created');

            return $this->response->setData($this->repository->skipPresenter(0)->find($license->id))
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function find($companyId, $id)
    {
        try {

            $license = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->find($id);

            return $this->response->setData($license)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @param array $data
     * @return Response
     */
    public function update($companyId, $id, array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $license = $this->repository->skipPresenter(1)->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->update([
                'status' => $data['status'],
                'starting' => $data['starting'],
                'ending' => $data['ending'],
                'type' => $data['type'],
                'icd' => $data['icd'],
            ], $id);

            if(isset($data['files_id']) && $data['files_id'])
                Media::setMyReference($data['files_id'], $license->id);

            $this->notificationLicense($license, 'updated');

            return $this->response->setData($this->repository->skipPresenter(0)->find($license->id))
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function delete($companyId, $id)
    {
        try {

            $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }
}