<?php

namespace OitentaOito\Services;

use OitentaOito\Repositories\StateRepository;

class StateService
{
    /**
     * @var StateRepository
     */
    protected $repository;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * StateService constructor.
     * @param StateRepository $repository
     * @param ResponseService $response
     */
    public function __construct(StateRepository $repository, ResponseService $response)
    {
        $this->repository = $repository;
        $this->response = $response;
    }


    /**
     * @param array $data
     * @return Response
     */
    public function all(array $data)
    {
        $states = $this->repository->orderBy("name","asc")->all();
        return $this->response->setData($states)->respondWithSuccess();
    }
}