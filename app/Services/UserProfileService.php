<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Repositories\UserProfileRepository;
use OitentaOito\Validators\UserProfileValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class UserProfileService
{
    /**
     * @var UserProfileRepository
     */
    protected $repository;


    /**
     * @var UserProfileValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * @param UserProfileRepository $repository
     * @param UserProfileValidator $validator
     * @param ResponseService $response
     */
    public function __construct(UserProfileRepository $repository, UserProfileValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
        $this->userId  = Authorizer::getResourceOwnerId();
    }

    /**
     * @return mixed
     */
    protected function existsRegister()
    {
        return $this->repository
            ->skipPresenter(1)
            ->findByField('user_id', $this->userId)
            ->first();
    }


    /**
     * @param $limit
     * @return Response
     */
    public function all($limit)
    {
        $usersProfiles = $this->repository->paginate($limit);
        return $this->response->setData($usersProfiles)->respondWithSuccess();
    }


    /**
     * @param array $data
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function create(array $data)
    {
        try {

            if($this->existsRegister())
                $this->validator->setId($this->existsRegister()->id);

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $userProfile = $this->repository->skipPresenter(0)->updateOrCreate([
                'user_id' => $this->userId,
                'name' => $data['name'],
                'gender' => $data['gender'],
                'date_of_birth' => $data['date_of_birth'],
                'identity_document' => $data['identity_document'],
                'identity_document_state' => $data['identity_document_state'],
                'identity_document_dispatcher_organ' => $data['identity_document_dispatcher_organ'],
                'identity_document_dispatcher_date' => $data['identity_document_dispatcher_date'],
                'register_individual' => $data['register_individual'],
                'voter_id_card' => $data['voter_id_card'],
                'driving_licence' => $data['driving_licence'],
                'driving_licence_category' => $data['driving_licence_category'],
                'driving_licence_dispatcher_date' => $data['driving_licence_dispatcher_date'],
                'driving_licence_validate_date' => $data['driving_licence_validate_date'],
                'work_record_booklet' => $data['work_record_booklet'],
                'work_record_booklet_series' => $data['work_record_booklet_series'],
                'work_record_booklet_dispatcher_state' => $data['work_record_booklet_dispatcher_state'],
                'social_integration_program' => $data['social_integration_program'],
                'professional_identity' => $data['professional_identity'],
                'address' => $data['address'],
                'data_address' => $data['data_address'],
                'data_email' => $data['data_email'],
                'data_phone' => $data['data_phone'],
                'data_phone_emergency' => $data['data_phone_emergency'],
                'about_me' => $data['about_me'],
            ], [
                'user_id' => $this->userId
            ]);

            return $this->response->setData($userProfile)
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $id
     * @return Response
     */
    public function find($id)
    {
        try {

            $userProfile = $this->repository->find($id);
            return $this->response->setData($userProfile)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param array $data
     * @param $id
     * @return Response
     */
    public function update(array $data, $id)
    {
        try {

            $this->validator->setId($id);
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $userProfile = $this->repository->update([
                'name' => $data['name'],
                'gender' => $data['gender'],
                'date_of_birth' => $data['date_of_birth'],
                'identity_document' => $data['identity_document'],
                'identity_document_state' => $data['identity_document_state'],
                'identity_document_dispatcher_organ' => $data['identity_document_dispatcher_organ'],
                'identity_document_dispatcher_date' => $data['identity_document_dispatcher_date'],
                'register_individual' => $data['register_individual'],
                'voter_id_card' => $data['voter_id_card'],
                'driving_licence' => $data['driving_licence'],
                'driving_licence_category' => $data['driving_licence_category'],
                'driving_licence_dispatcher_date' => $data['driving_licence_dispatcher_date'],
                'driving_licence_validate_date' => $data['driving_licence_validate_date'],
                'work_record_booklet' => $data['work_record_booklet'],
                'work_record_booklet_series' => $data['work_record_booklet_series'],
                'work_record_booklet_dispatcher_state' => $data['work_record_booklet_dispatcher_state'],
                'social_integration_program' => $data['social_integration_program'],
                'professional_identity' => $data['professional_identity'],
                'address' => $data['address'],
                'data_address' => $data['data_address'],
                'data_email' => $data['data_email'],
                'data_phone' => $data['data_phone'],
                'data_phone_emergency' => $data['data_phone_emergency'],
                'about_me' => $data['about_me'],
            ], $id);

            return $this->response->setData($userProfile)
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $id
     * @return Response
     */
    public function delete($id)
    {
        try {

            $this->repository->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }

}