<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use OitentaOito\Repositories\UserAdditionalRepository;
use OitentaOito\Validators\UserAdditionalValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class UserAdditionalService
{
    /**
     * @var UserAdditionalRepository
     */
    protected $repository;


    /**
     * @var UserAdditionalValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * @param UserAdditionalRepository $repository
     * @param UserAdditionalValidator $validator
     * @param ResponseService $response
     */
    public function __construct(UserAdditionalRepository $repository, UserAdditionalValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param array $discounts
     * @return array
     */
    protected function discountsCollection(array $discounts)
    {
        $arr = [];
        for($i = 0; $i <= count($discounts) - 1; $i++) {
            $arr[$discounts[$i]['benefit_id']] = ['type' => $discounts[$i]['type'], 'value' => $discounts[$i]['value']];
        }

        return $arr;
    }


    /**
     * @param $companyId
     * @param $limit
     * @return Response
     */
    public function all($companyId, $limit)
    {
        $usersAdditionals = $this->repository->scopeQuery(function($query) use ($companyId) {
            return $query->where('company_id', $companyId);
        })
            ->paginate($limit);

        return $this->response->setData($usersAdditionals)->respondWithSuccess();
    }


    /**
     * @param $companyId
     * @param array $data
     * @return Response
     */
    public function create($companyId, array $data)
    {
        try {

            $this->validator->setWhere('id','company_id',$companyId);
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $userAdditional = $this->repository->skipPresenter(1)->updateOrCreate([
                'user_id' => $data['user_id'],
                'company_id' => $companyId,
                'department_id' => $data['department_id'],
                'position_job_id' => $data['position_job_id'],
                'education_level_id' => $data['education_level_id'],
                'admission_date' => $data['admission_date'],
                'vacation_balance_days' => (isset($data['vacation_balance_days']) && $data['vacation_balance_days'] ? $data['vacation_balance_days'] : 0),
                'base_salary' => $data['base_salary'],
                'payday' => $data['payday'],
                'bank_data' => $data['bank_data'],
                'special_needs' => (isset($data['special_needs']) && $data['special_needs'] ? $data['special_needs'] : null),
            ], [
                'user_id' => $data['user_id'], 'company_id' => $companyId
            ]);

            $userAdditional->discounts()->sync($this->discountsCollection($data['discounts']), false);

            return $this->response->setData($this->repository->skipPresenter(0)->find($userAdditional->id))
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function find($companyId, $id)
    {
        try {

            $userAdditional = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->find($id);

            return $this->response->setData($userAdditional)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @param array $data
     * @return Response
     */
    public function update($companyId, $id, array $data)
    {
        try {

            $this->validator->setId($id);
            $this->validator->setWhere('id','company_id',$companyId);
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $userAdditional = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->update([
                'department_id' => $data['department_id'],
                'position_job_id' => $data['position_job_id'],
                'education_level_id' => $data['education_level_id'],
                'admission_date' => $data['admission_date'],
                'base_salary' => $data['base_salary'],
                'payday' => $data['payday'],
                'bank_data' => $data['bank_data'],
                'special_needs' => (isset($data['special_needs']) && $data['special_needs'] ? $data['special_needs'] : null),
            ], $id);

            $userAdditional->discounts()->sync($this->discountsCollection($data['discounts']), false);

            return $this->response->setData($userAdditional)
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function delete($companyId, $id)
    {
        try {

            $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }
}