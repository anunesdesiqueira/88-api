<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\MessageBag;
use OitentaOito\Jobs\SendResetLinkEmail;
use OitentaOito\Repositories\UserRepository;
use OitentaOito\Validators\ReminderValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class ReminderService
{
    /**
     * @var UserRepository
     */
    protected $repository;


    /**
     * @var ReminderValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * ReminderService constructor.
     * @param UserRepository $repository
     * @param ReminderValidator $validator
     * @param ResponseService $response
     */
    public function __construct(UserRepository $repository, ReminderValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param $email
     */
    protected function SendResetLinkEmail($email)
    {
        dispatch((
            new SendResetLinkEmail($this->repository->skipPresenter(1)->findByField('email', $email)[0])
        ));
    }


    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);
        $user->save();
    }


    /**
     * @param $email
     * @return Response
     */
    public function forgot($email)
    {
        try {

            $this->validator->with(['email' => $email])->passesOrFail(ValidatorInterface::RULE_CREATE);
            $this->SendResetLinkEmail($email);

            return $this->response->setMessage(trans("messages.success.forget_password_request"))
                        ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param array $data
     * @return Response
     */
    public function reset(array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $response = Password::reset($data, function ($user, $password) {
                $this->resetPassword($user, $password);
            });

            switch ($response) {
                case Password::PASSWORD_RESET:
                    return $this->response->setMessage(trans("messages.success.new_password_set"))
                        ->respondWithSuccess();
                default:
                    throw new ValidatorException(new MessageBag(['invalid' => trans("messages.errors.user_not_found")]));
            }

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }

}