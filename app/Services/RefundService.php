<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Entities\Media;
use OitentaOito\Validators\RefundValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use OitentaOito\Repositories\RefundRepository;

class RefundService
{
    /**
     * @var RefundRepository
     */
    protected $repository;


    /**
     * @var RefundValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * RefundService constructor.
     * @param RefundRepository $repository
     * @param RefundValidator $validator
     * @param ResponseService $response
     */
    public function __construct(RefundRepository $repository, RefundValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param $companyId
     * @param $limit
     * @return Response
     */
    public function all($companyId, $limit)
    {

        $refunds = $this->repository->scopeQuery(function($query) use ($companyId) {
            return $query->where('company_id', $companyId);
        })
            ->orderBy('created_at', 'desc')
            ->paginate($limit);

        return $this->response->setData($refunds)->respondWithSuccess();
    }


    /**
     * @param $companyId
     * @param array $data
     * @return Response
     */
    public function create($companyId, array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $refund = $this->repository->skipPresenter(1)->create([
                'created_user_id' => Authorizer::getResourceOwnerId(),
                'user_id' => $data['user_id'],
                'company_id' => $companyId,
                'type_expenditure_id' => $data['type_expenditure_id'],
                'status' => $data['status'],
                'date' => $data['date'],
                'value' => $data['value'],
                'amount_approved' => (isset($data['amount_approved']) && $data['amount_approved']? $data['amount_approved'] : null),
                'description' => $data['description'],
                'justification' => (isset($data['justification']) && $data['justification']? $data['justification'] : null),
            ]);

            if(isset($data['file_returnable_id']) && $data['file_returnable_id'])
                Media::setMyReference($data['file_returnable_id'], $refund->id);

            if(isset($data['file_receipt_id']) && $data['file_receipt_id'])
                Media::setMyReference($data['file_receipt_id'], $refund->id);

            return $this->response->setData($this->repository->skipPresenter(0)->find($refund->id))
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function find($companyId, $id)
    {
        try {

            $refund = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->find($id);

            return $this->response->setData($refund)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @param array $data
     * @return Response
     */
    public function update($companyId, $id, array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $refund = $this->repository->skipPresenter(1)->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->update([
                'type_expenditure_id' => $data['type_expenditure_id'],
                'status' => $data['status'],
                'date' => $data['date'],
                'value' => $data['value'],
                'amount_approved' => $data['amount_approved'],
                'description' => $data['description'],
                'justification' => $data['justification'],
            ], $id);

            if(isset($data['file_returnable_id']) && $data['file_returnable_id'])
                Media::setMyReference($data['file_returnable_id'], $refund->id);

            if(isset($data['file_receipt_id']) && $data['file_receipt_id'])
                Media::setMyReference($data['file_receipt_id'], $refund->id);

            return $this->response->setData($this->repository->skipPresenter(0)->find($refund->id))
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function delete($companyId, $id)
    {
        try {

            $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }
}