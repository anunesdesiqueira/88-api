<?php

namespace OitentaOito\Services;

use Illuminate\Support\MessageBag;

class ResponseService
{
    protected $statusCode = 200;
    protected $data = [];
    protected $message = [];

    /**
     * Getter for statusCode
     *
     * @return mixed
     */
    public function getStatusCode() {
        return $this->statusCode;
    }

    /**
     * Setter for statusCode
     *
     * @param int $statusCode Value to set
     *
     * @return self
     */
    public function setStatusCode( $statusCode ) {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * Getter for data
     *
     * @return mixed
     */
    public function getData() {
        return $this->data;
    }

    /**
     * Setter for data
     *
     * @param array $data Value to set
     *
     * @return self
     */
    public function setData( $data ) {
        $this->data = $data;
        return $this;
    }

    /**
     * Getter for message
     *
     * @return mixed
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * Setter for message
     *
     * @param string
     *
     * @return self $message
     */
    public function setMessage( $message ) {
        $this->message = [ "message" => $message ];
        return $this;
    }

    /**
     * @param array $array
     * @param array $headers
     * @return mixed
     */
    protected function respondWithArray( array $array , array $headers = [] ) {
        return \Response::json( $array , $this->statusCode , $headers );
    }

    /**
     * Generates a Response with a 200 HTTP header and a given message.
     *
     * @return  Response
     */
    public function respondWithSuccess() {
        $response = array_merge( [ "success" => true ] , $this->message , $this->data );
        return $this->respondWithArray( $response );
    }

    /**
     * Generates a Response with a 403 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorForbidden( $message = "Acesso bloqueado... :(" ) {
        $messageBag = new MessageBag( [ 'message' => $message ] );
        return $this->errorValidation( $messageBag , 403 );
    }

    /**
     * Generates a Response with a 500 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorInternalError( $message = "Erro Interno" ) {
        $messageBag = new MessageBag( [ 'message' => $message ] );
        return $this->errorValidation( $messageBag , 500 );
    }

    /**
     * Generates a Response with a 404 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorNotFound( $message = "Recurso não encontrado" ) {
        $messageBag = new MessageBag( [ 'message' => $message ] );
        return $this->errorValidation( $messageBag , 404 );
    }

    /**
     * Generates a Response with a 401 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorUnauthorized( $message = "Não Autorizado" ) {
        $messageBag = new MessageBag( [ 'message' => $message ] );
        return $this->errorValidation( $messageBag , 401 );
    }

    /**
     * Generates a Response with a 400 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorWrongArgs( $message = "Argumento Inválido" ) {
        $messageBag = new MessageBag( [ 'message' => $message ] );
        return $this->errorValidation( $messageBag , 400 );
    }

    /**
     * Generates a Response with a 422 HTTP header and a given message errros.
     *
     * @param MessageBag $errors
     * @param int $statusCode
     * @return Response
     */
    public function errorValidation( MessageBag $errors , $statusCode = 422 ) {
        return $this->setStatusCode($statusCode)->respondWithArray( [ 'errors' => $errors->toArray() ] );
    }

    /**
     * Generates a Response with a 422 HTTP header and no errors message.
     *
     * @return  Response
     */
    public function simpleErrorValidation( $statusCode = 422 ) {
        return $this->setStatusCode($statusCode)->respondWithArray( [] );
    }
}