<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Criteria\DocumentCriteria;
use OitentaOito\Entities\Media;
use OitentaOito\Repositories\MediaRepository;
use OitentaOito\Validators\DocumentValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class DocumentService
{
    /**
     * @var DocumentRepository
     */
    protected $repository;


    /**
     * @var DocumentValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * DocumentService constructor.
     * @param MediaRepository $repository
     * @param DocumentValidator $validator
     * @param ResponseService $response
     */
    public function __construct(MediaRepository $repository, DocumentValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param $companyId
     * @param $limit
     * @return Response
     */
    public function all($companyId, $limit)
    {
        $this->repository->pushCriteria(DocumentCriteria::class);
        $documents = $this->repository->scopeQuery(function($query) use ($companyId) {
            return $query->where('company_id', $companyId);
        })
            ->orderBy('created_at', 'desc')
            ->paginate($limit);

        return $this->response->setData($documents)->respondWithSuccess();
    }


    /**
     * @param $companyId
     * @param array $data
     * @return Response
     */
    public function create($companyId, array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $document = $this->repository->skipPresenter(1)->create([
                'created_user_id' => Authorizer::getResourceOwnerId(),
                'company_id' => $companyId,
                'user_id' => (isset($data['user_id']) && $data['user_id'] ? $data['user_id'] : null),
                'reference' => (isset($data['reference']) && $data['reference'] ? $data['reference'] : null),
                'slug' => $data['slug'],
                'name' => $data['name'],
                'folder' => $data['folder'],
                'access' => $data['access'],
                'file' => $data['file'],
            ]);

            return $this->response->setData($this->repository->skipPresenter(0)->find($document->id))
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function find($companyId, $id)
    {
        try {

            $this->repository->pushCriteria(DocumentCriteria::class);
            $document = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->find($id);

            return $this->response->setData($document)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @param array $data
     * @return Response
     */
    public function update($companyId, $id, array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $this->repository->pushCriteria(DocumentCriteria::class);
            $document = $this->repository->skipPresenter(1)->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->update([
                'user_id' => (isset($data['user_id']) && $data['user_id'] ? $data['user_id'] : null),
                'reference' => (isset($data['reference']) && $data['reference'] ? $data['reference'] : null),
                'name' => $data['name'],
                'folder' => $data['folder'],
                'access' => $data['access'],
                'file' => $data['file'],
            ], $id);

            return $this->response->setData($this->repository->skipPresenter(0)->find($document->id))
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function delete($companyId, $id)
    {
        try {

            $this->repository->pushCriteria(DocumentCriteria::class);
            $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }
}