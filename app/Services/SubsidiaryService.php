<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Validators\SubsidiaryValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use OitentaOito\Repositories\SubsidiaryRepository;

class SubsidiaryService
{
    /**
     * @var SubsidiaryRepository
     */
    protected $repository;


    /**
     * @var SubsidiaryValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * @param SubsidiaryRepository $repository
     * @param SubsidiaryValidator $validator
     * @param ResponseService $response
     */
    public function __construct(SubsidiaryRepository $repository, SubsidiaryValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param $companyId
     * @param $limit
     * @return Response
     */
    public function all($companyId, $limit)
    {
        $subsidiaries = $this->repository->scopeQuery(function($query) use ($companyId) {
            return $query->where('parent', $companyId);
        })
            ->orderBy('created_at', 'desc')
            ->paginate($limit);

        return $this->response->setData($subsidiaries)->respondWithSuccess();
    }


    /**
     * @param $companyId
     * @param array $data
     * @return Response
     */
    public function create($companyId, array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $subsidiary = $this->repository->skipPresenter()->create([
                'parent' => $companyId,
                'type' => 'subsidiary',
                'trading_name' => $data['trading_name'],
                'national_register_of_legal_entities' => $data['national_register_of_legal_entities'],
                'address' => $data['address'],
                'data_address' => $data['data_address'],
                'data_email' => $data['data_email'],
                'data_phone' => $data['data_phone'],
            ]);

            $subsidiary->users()->attach(Authorizer::getResourceOwnerId(), ['role_id' => 1]);

            return $this->response->setData($this->repository->skipPresenter(0)->find($subsidiary->id))
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function find($companyId, $id)
    {
        try {

            $subsidiary = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('parent', $companyId);
            })->find($id);

            return $this->response->setData($subsidiary)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @param array $data
     * @return Response
     */
    public function update($companyId, $id, array $data)
    {
        try {
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $subsidiary = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('parent', $companyId);
            })->update([
                'type' => 'subsidiary',
                'trading_name' => $data['trading_name'],
                'national_register_of_legal_entities' => $data['national_register_of_legal_entities'],
                'address' => $data['address'],
                'data_address' => $data['data_address'],
                'data_email' => $data['data_email'],
                'data_phone' => $data['data_phone'],
            ], $id);

            return $this->response->setData($subsidiary)
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function delete($companyId, $id)
    {
        try {

            $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('parent', $companyId);
            })->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }
}