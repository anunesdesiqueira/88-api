<?php

namespace OitentaOito\Services;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use OitentaOito\Jobs\SendInvitationMail;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use OitentaOito\Repositories\InvitationRepository;
use OitentaOito\Validators\InvitationValidator;

class InvitationService
{
    /**
     * @var InvitationRepository
     */
    protected $repository;


    /**
     * @var InvitationValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * InvitationService constructor.
     * @param InvitationRepository $repository
     * @param InvitationValidator $validator
     * @param ResponseService $response
     */
    public function __construct(InvitationRepository $repository, InvitationValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param $id
     */
    private function sendInvitationMail($id)
    {
        dispatch((new SendInvitationMail(
            $this->repository->skipPresenter()->find($id))
        ));
    }


    /**
     * @param $invitationCode
     * @param $userId
     * @return Response
     */
    public function accepted($invitationCode, $userId)
    {
        try {

            $invitation = $this->repository->skipPresenter(1)->scopeQuery(function($query) {
                return $query->where('accepted', 0)
                             ->whereNull('accepted_at');
            })
                ->findByField('invitation_code', $invitationCode)
                ->first();

            if(is_null($invitation) || is_null($userId))
                throw new ValidatorException(new MessageBag(['general' => trans( "messages.errors.model_not_found")]));

            $invitation->user_id = $userId;
            $invitation->invitation_code = null;
            $invitation->accepted = 1;
            $invitation->accepted_at = Carbon::now();

            $userAdditional = app('OitentaOito\Entities\UserAdditional');
            $userAdditional->create([
                'user_id' => $invitation->user_id,
                'company_id' => $invitation->company_id,
                'department_id' => $invitation->department_id,
                'position_job_id' => $invitation->position_job_id,
            ]);

            $company = app('OitentaOito\Entities\Company');
            $company->find($invitation->company_id)->users()
                ->attach($invitation->user_id, ['role_id' => $invitation->role_id]);

            return $this->update($invitation->company_id, $invitation->id, $invitation->toArray());

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $limit
     * @return Response
     */
    public function all($companyId, $limit)
    {
        $invitations = $this->repository->scopeQuery(function($query) use ($companyId) {
            return $query->where('company_id', $companyId);
        })->paginate($limit);

        return $this->response->setData($invitations)->respondWithSuccess();
    }


    /**
     * @param $companyId
     * @param array $data
     * @return Response
     */
    public function create($companyId, array $data)
    {
        try {

            $this->validator->setWhere('id','company_id',$data['company_id']);
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $invitation = $this->repository->create([
                'company_id' => (int) $data['company_id'],
                'department_id' => (int) $data['department_id'],
                'position_job_id' => (int) $data['position_job_id'],
                'role_id' => $data['role_id'],
                'email' => $data['email'],
                'name' => $data['name'],
                'invitation_code' => isset($data['invitation_code'])? $data['invitation_code'] : str_random(60),
            ]);

            $this->sendInvitationMail($invitation['data']['id']);

            return $this->response->setData($invitation)
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function find($companyId, $id)
    {
        try {

            $invitation = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->find($id);

            return $this->response->setData($invitation)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @param array $data
     * @return Response
     */
    public function update($companyId, $id, array $data)
    {
        try {

            $this->validator->setId($id);
            $this->validator->setWhere('id','company_id',$companyId);
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $invitation = $this->repository->skipPresenter(0)->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->update([
                'user_id' => $data['user_id'] ? (int) $data['user_id'] : null,
                'department_id' => (int) $data['department_id'],
                'position_job_id' => (int) $data['position_job_id'],
                'role_id' => $data['role_id'],
                'email' => $data['email'],
                'name' => $data['name'],
                'invitation_code' => $data['invitation_code'],
                'accepted' => $data['accepted'],
                'accepted_at' => $data['accepted_at'],
            ], $id);

            return $this->response->setData($invitation)
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function delete($companyId, $id)
    {
        try {

            $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }
}