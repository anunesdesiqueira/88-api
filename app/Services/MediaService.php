<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Validators\MediaValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use OitentaOito\Repositories\MediaRepository;

class MediaService
{
    /**
     * @var MediaRepository
     */
    protected $repository;


    /**
     * @var MediaValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * @param MediaRepository $repository
     * @param MediaValidator $validator
     * @param ResponseService $response
     */
    public function __construct(MediaRepository $repository, MediaValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param $limit
     * @return Response
     */
    public function all($limit)
    {
        $medias = $this->repository->orderBy('created_at', 'desc')->paginate($limit);
        return $this->response->setData($medias)->respondWithSuccess();
    }


    /**
     * @param array $data
     * @return Response
     */
    public function create(array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $media = $this->repository->create([
                'created_user_id' => Authorizer::getResourceOwnerId(),
                'user_id' => (isset($data['user_id']) && $data['user_id'] ? $data['user_id'] : null),
                'reference' => (isset($data['reference']) && $data['reference'] ? $data['reference'] : null),
                'slug' => $data['slug'],
                'name' => (isset($data['name']) && $data['name'] ? $data['name'] : null),
                'folder' => (isset($data['folder']) && $data['folder'] ? $data['folder'] : 'personal'),
                'access' => (isset($data['access']) && $data['access'] ? $data['access'] : 'private'),
                'file' => $data['file'],
            ]);

            return $this->response->setData($media)
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $id
     * @return Response
     */
    public function find($id)
    {
        try {

            $media = $this->repository->find($id);
            return $this->response->setData($media)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param array $data
     * @param $id
     * @return Response
     */
    public function update(array $data, $id)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $license = $this->repository->update([
                'name' => (isset($data['name']) && $data['name'] ? $data['name'] : null),
                'folder' => (isset($data['folder']) && $data['folder'] ? $data['folder'] : 'personal'),
                'access' => (isset($data['access']) && $data['access'] ? $data['access'] : 'private'),
                'file' => $data['file'],
            ], $id);

            return $this->response->setData($license)
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $id
     * @return Response
     */
    public function delete($id)
    {
        try {

            $this->repository->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }
}