<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use OitentaOito\Validators\TaxFrameworkValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use OitentaOito\Repositories\TaxFrameworkRepository;

class TaxFrameworkService
{
    /**
     * @var TaxFrameworkRepository
     */
    protected $repository;


    /**
     * @var TaxFrameworkValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * @param TaxFrameworkRepository $repository
     * @param TaxFrameworkValidator $validator
     * @param ResponseService $response
     */
    public function __construct(TaxFrameworkRepository $repository, TaxFrameworkValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param $limit
     * @return Response
     */
    public function all($limit)
    {
        $taxesFrameworks = $this->repository->paginate($limit);
        return $this->response->setData($taxesFrameworks)->respondWithSuccess();
    }


    /**
     * @param array $data
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function create(array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $taxFramework = $this->repository->create([
                'name' => $data['name'],
                'slug' => $data['name'],
            ]);

            return $this->response->setData($taxFramework)
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $id
     * @return Response
     */
    public function find($id)
    {
        try {

            $taxFramework = $this->repository->find($id);
            return $this->response->setData($taxFramework)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param array $data
     * @param $id
     * @return Response
     */
    public function update(array $data, $id)
    {
        try {
            $this->validator->setId($id);
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $taxFramework = $this->repository->update([
                'name' => $data['name'],
                'slug' => $data['name'],
            ], $id);

            return $this->response->setData($taxFramework)
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $id
     * @return Response
     */
    public function delete($id)
    {
        try {

            $this->repository->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }

}