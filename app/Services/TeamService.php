<?php

namespace OitentaOito\Services;

use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Repositories\UserRepository;
use League\Fractal\Manager;

class TeamService
{
    /**
     * @var UserRepository
     */
    protected $repository;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * @var Manager
     */
    protected $manager;


    /**
     * TeamService constructor.
     * @param UserRepository $repository
     * @param ResponseService $response
     * @param Manager $manager
     */
    public function __construct(UserRepository $repository, ResponseService $response, Manager $manager)
    {
        $this->repository = $repository;
        $this->response = $response;
        $this->manager = $manager;
    }


    /**
     * @param $limit
     * @param $companyId
     * @return Response
     */
    public function all($limit, $companyId)
    {
        $users = $this->repository->whereHas('companies', function($query) use ($companyId) {
            return $query->where('company_id', $companyId);
        })
            ->orderBy('email', 'asc')
            ->paginate($limit);

        return $this->response->setData($users)->respondWithSuccess();
    }
}