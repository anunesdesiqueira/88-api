<?php

namespace OitentaOito\Services;

use Illuminate\Support\Facades\Auth;
use OitentaOito\Repositories\UserRepository;
use League\OAuth2\Server\Exception;
use OitentaOito\Traits\GoogleTrait;

class PasswordGrantVerifier 
{
    use GoogleTrait;

    /**
     * @var UserRepository
     */
    protected $repository;


    /**
     * The HTTP Client instance.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;


    /**
     * PasswordGrantVerifier constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * @param $google
     * @return mixed
     */
    protected function updateOrCreate($google)
    {
        $user =  $this->repository->skipPresenter(1)->updateOrCreate([
            'email' => $google['email'],
            'google_id' => $google['user_id'],
            'terms_of_use' => 1,
            'activated' => 1
        ], [
            'email' => $google['email']
        ]);

        return $user;
    }


    /**
     * @param $code
     * @return bool
     * @throws Exception\InvalidCredentialsException
     */
    protected function connectGoogle($code)
    {
        try {
            $google = $this->getUserForCode($code);
            $user = $this->updateOrCreate($google);

            return $user->id;

        } catch (\Exception $e) {
            throw new Exception\InvalidCredentialsException();
        }
    }


    /**
     * @param $username
     * @return mixed
     * @throws Exception\InvalidCredentialsException
     */
    protected function emailOrRegisterIndividual($username)
    {
        if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
            return $username;
        }

        $username = preg_replace('/[^0-9]/', '', $username);

        if(filter_var($username, FILTER_VALIDATE_INT)) {
            $user = $this->repository->skipPresenter(1)->whereHas('profile', function($query) use ($username) {
                $query->where(['register_individual' => $username]);
            });

            if(!isset($user[0])) {
                throw new Exception\InvalidCredentialsException();
            }

            return $user[0]->email;
        }

        return $username;
    }


    /**
     * @param $username
     * @param $password
     * @param $code
     * @return bool
     */
    public function verify($username, $password, $code)
    {
        if($code && Auth::loginUsingId($this->connectGoogle($code))) {
            return Auth::user()->id;
        }

        if (Auth::attempt(['email' => $this->emailOrRegisterIndividual($username), 'password' => $password, 'terms_of_use' => 1, 'activated' => 1])) {
            return Auth::user()->id;
        }

        return false;
    }
}