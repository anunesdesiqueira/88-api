<?php

namespace OitentaOito\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Entities\Media;
use OitentaOito\Validators\AbsenceValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use OitentaOito\Repositories\AbsenceRepository;

class AbsenceService
{
    /**
     * @var AbsenceRepository
     */
    protected $repository;


    /**
     * @var AbsenceValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * AbsenceService constructor.
     * @param AbsenceRepository $repository
     * @param AbsenceValidator $validator
     * @param ResponseService $response
     */
    public function __construct(AbsenceRepository $repository, AbsenceValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param $companyId
     * @param $limit
     * @return Response
     */
    public function all($companyId, $limit)
    {
        $absences = $this->repository->scopeQuery(function($query) use ($companyId) {
            return $query->where('company_id', $companyId);
        })
            ->orderBy('created_at', 'desc')
            ->paginate($limit);

        return $this->response->setData($absences)->respondWithSuccess();
    }


    /**
     * @param $companyId
     * @param array $data
     * @return Response
     */
    public function create($companyId, array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $absence = $this->repository->skipPresenter(1)->create([
                'created_user_id' => Authorizer::getResourceOwnerId(),
                'user_id' => $data['user_id'],
                'company_id' => $companyId,
                'status' => $data['status'],
                'starting' => $data['starting'],
                'ending' => $data['ending'],
                'comment' => $data['comment'],
                'justified' => $data['justified'],
            ]);

            if(isset($data['file_id']) && $data['file_id'])
                Media::setMyReference($data['file_id'], $absence->id);

            return $this->response->setData($this->repository->skipPresenter(0)->find($absence->id))
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function find($companyId, $id)
    {
        try {
            $absence = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->find($id);

            return $this->response->setData($absence)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @param array $data
     * @return Response
     */
    public function update($companyId, $id, array $data)
    {
        try {
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $absence = $this->repository->skipPresenter(1)->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->update([
                'status' => $data['status'],
                'starting' => $data['starting'],
                'ending' => $data['ending'],
                'comment' => $data['comment'],
                'justified' => $data['justified'],
            ], $id);

            if(isset($data['file_id']) && $data['file_id'])
                Media::setMyReference($data['file_id'], $absence->id);

            return $this->response->setData($this->repository->skipPresenter(0)->find($absence->id))
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function delete($companyId, $id)
    {
        try {

            $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }
}