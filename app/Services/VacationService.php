<?php

namespace OitentaOito\Services;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use OitentaOito\Repositories\VacationRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use OitentaOito\Validators\VacationValidator;

class VacationService
{
    /**
     * @var VacationRepository
     */
    protected $repository;


    /**
     * @var VacationValidator
     */
    protected $validator;


    /**
     * @var ResponseService
     */
    protected $response;


    /**
     * VacationService constructor.
     * @param VacationRepository $repository
     * @param VacationValidator $validator
     * @param ResponseService $response
     */
    public function __construct(VacationRepository $repository, VacationValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
    }


    /**
     * @param $starting
     * @param $ending
     * @return mixed
     */
    private function diffInDays($starting, $ending)
    {
        $starting = Carbon::parse($starting);
        $ending = Carbon::parse($ending);

        return $starting->diffInDays($ending);
    }


    /**
     * @param $companyId
     * @param $limit
     * @return Response
     */
    public function all($companyId, $limit)
    {
        $vacations = $this->repository->scopeQuery(function($query) use ($companyId) {
            return $query->where('company_id', $companyId);
        })
            ->orderBy('created_at', 'desc')
            ->paginate($limit);

        return $this->response->setData($vacations)->respondWithSuccess();
    }


    /**
     * @param $companyId
     * @param array $data
     * @return Response
     */
    public function create($companyId, array $data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $vacation = $this->repository->create([
                'type' => $data['type'],
                'created_user_id' => Authorizer::getResourceOwnerId(),
                'company_id' => $companyId,
                'user_id' => (isset($data['user_id']) && $data['user_id'] ? $data['user_id'] : null),
                'department_id' => (isset($data['department_id']) && $data['department_id'] ? $data['department_id'] : null),
                'vacation_bonus_days' => (isset($data['vacation_bonus_days']) && $data['vacation_bonus_days'] ? $data['vacation_bonus_days'] : null),
                'name' => (isset($data['name']) && $data['name'] ? $data['name'] : null),
                'status' => $data['status'],
                'starting' => (isset($data['starting']) && $data['starting'] ? $data['starting'] : null),
                'ending' => (isset($data['ending']) &&  $data['ending'] ? $data['ending'] : null),
                'days' => (isset($data['starting']) && isset($data['ending']) && $data['starting'] && $data['ending']? $this->diffInDays($data['starting'], $data['ending']) : $data['vacation_bonus_days']),
            ]);

            return $this->response->setData($vacation)
                ->setMessage(trans("messages.success.created"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function find($companyId, $id)
    {
        try {

            $vacation = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->find($id);

            return $this->response->setData($vacation)->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @param array $data
     * @return Response
     */
    public function update($companyId, $id, array $data)
    {
        try {
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $license = $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->update([
                'user_id' => (isset($data['user_id']) && $data['user_id'] ? $data['user_id'] : null),
                'department_id' => (isset($data['department_id']) && $data['department_id'] ? $data['department_id'] : null),
                'vacation_bonus_days' => (isset($data['vacation_bonus_days']) && $data['vacation_bonus_days'] ? $data['vacation_bonus_days'] : null),
                'status' => $data['status'],
                'starting' => (isset($data['starting']) && $data['starting'] ? $data['starting'] : null),
                'ending' => (isset($data['ending']) &&  $data['ending'] ? $data['ending'] : null),
                'days' => (isset($data['starting']) && $data['ending']? $this->diffInDays($data['starting'], $data['ending']) : null),
            ], $id);

            return $this->response->setData($license)
                ->setMessage(trans("messages.success.update"))
                ->respondWithSuccess();

        } catch (ValidatorException $e) {
            return $this->response->errorValidation($e->getMessageBag());
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }


    /**
     * @param $companyId
     * @param $id
     * @return Response
     */
    public function delete($companyId, $id)
    {
        try {

            $this->repository->scopeQuery(function($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->delete($id);

            return $this->response->setMessage(trans("messages.success.destroy_register"))
                ->respondWithSuccess();

        } catch (ModelNotFoundException $e) {
            return $this->response->errorValidation(
                new MessageBag(['general' => trans("messages.errors.model_not_found")])
            );
        }
    }
}