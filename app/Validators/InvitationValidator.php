<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class InvitationValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'department_id' => 'required|integer|exists:departments,id',
            'position_job_id' => 'required|integer|exists:positions_jobs,id',
            'role_id' => 'required|integer|exists:roles,id',
            'email' => 'required|email|unique:invitations,email',
            'name' => 'required|max:255',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'department_id' => 'required|integer|exists:departments,id',
            'position_job_id' => 'required|integer|exists:positions_jobs,id',
            'role_id' => 'required|integer|exists:roles,id',
            'email' => 'required|email|unique:invitations,email',
            'name' => 'required|max:255',
        ],
    ];

}
