<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class VacationValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'type' => 'required|in:company,department,user,vacation_bonus',
            'user_id' => 'required_if:type,user|integer|exists:users,id',
            'department_id' => 'required_if:type,department|integer|exists:departments,id',
            'vacation_bonus_days' => 'required_if:type,vacation_bonus|integer',
            'name' => 'required_unless:type,vacation_bonus|max:255',
            'status' => 'required|in:requested,approved,refused',
            'starting' => 'required_unless:type,vacation_bonus|date',
            'ending' => 'required_unless:type,vacation_bonus|date|after:starting',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'vacation_bonus_days' => 'required_if:type,vacation_bonus|integer',
            'status' => 'required|in:requested,approved,refused',
            'starting' => 'required_unless:type,vacation_bonus|date',
            'ending' => 'required_unless:type,vacation_bonus|date|after:starting',
        ],
   ];
}
