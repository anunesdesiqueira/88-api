<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class UserAdditionalValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'department_id' => 'required|integer|exists:departments,id',
            'position_job_id' => 'required|integer|exists:positions_jobs,id',
            'education_level_id' => 'required|integer|exists:educations_levels,id',
            'admission_date' => 'required|date',
            'base_salary' => 'required',
            'payday.*' => 'required',
            'discounts.*.benefit_id'  => 'required_if:discounts_nope,0',
            'discounts.*.type'  => 'required_if:discounts_nope,0|in:percentage,currency',
            'discounts.*.value'  => 'required_if:discounts_nope,0',
            'bank_data.*.type' => 'required',
            'bank_data.*.bank' => 'required',
            'bank_data.*.agency' => 'required',
            'bank_data.*.account' => 'required',
            'special_needs.*.type' => 'required_if:special_needs_nope,0',
            'special_needs.*.pne' => 'required_if:special_needs_nope,0',
        ],

        ValidatorInterface::RULE_UPDATE => [
            'department_id' => 'required|integer|exists:departments,id',
            'position_job_id' => 'required|integer|exists:positions_jobs,id',
            'education_level_id' => 'required|integer|exists:educations_levels,id',
            'admission_date' => 'required|date',
            'base_salary' => 'required',
            'payday.*' => 'required',
            'discounts.*.benefit_id'  => 'required_if:discounts_nope,0',
            'discounts.*.type'  => 'required_if:discounts_nope,0|in:percentage,currency',
            'discounts.*.value'  => 'required_if:discounts_nope,0',
            'bank_data.*.type' => 'required',
            'bank_data.*.bank' => 'required',
            'bank_data.*.agency' => 'required',
            'bank_data.*.account' => 'required',
            'special_needs.*.type' => 'required_if:special_needs_nope,0',
            'special_needs.*.pne' => 'required_if:special_needs_nope,0',
        ],
   ];
}
