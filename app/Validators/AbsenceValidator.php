<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class AbsenceValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'user_id' => 'required|integer|exists:users,id',
            'status' => 'required|in:requested,approved,refused',
            'starting' => 'required|date',
            'ending' => 'required|date|after:starting',
            'comment' => 'required',
            'justified' => 'boolean',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'status' => 'required|in:requested,approved,refused',
            'starting' => 'required|date',
            'ending' => 'required|date|after:starting',
            'comment' => 'required',
            'justified' => 'boolean',
        ],
   ];
}
