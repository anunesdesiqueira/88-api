<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class UserValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email'         => 'required|email|unique:users,email|max:255',
            'password'      => 'required|min:6|confirmed',
            'terms_of_use'  => 'required|accepted',
        ],

        ValidatorInterface::RULE_UPDATE => [
            'email'         => 'required|email|unique:users,email|max:255',
        ],
   ];
}
