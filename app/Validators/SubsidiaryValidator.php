<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class SubsidiaryValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'trading_name' => 'required|string|max:255',
            'national_register_of_legal_entities' => 'required|unique:companies,national_register_of_legal_entities',
//            'address' => 'required',
//            'data_address.*.street' => 'required',
//            'data_address.*.number' => 'required',
//            'data_address.*.neighborhood' => 'required',
//            'data_address.*.state' => 'required',
//            'data_address.*.city' => 'required',
//            'data_address.*.postal_code' => 'required',
//            'data_address.*.country' => 'required',
            'data_email.*.email' => 'required|email',
            'data_phone.*.phone' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'trading_name' => 'required|string|max:255',
            'national_register_of_legal_entities' => 'required|unique:companies,national_register_of_legal_entities',
//            'address' => 'required',
//            'data_address.*.street' => 'required',
//            'data_address.*.number' => 'required',
//            'data_address.*.neighborhood' => 'required',
//            'data_address.*.state' => 'required',
//            'data_address.*.city' => 'required',
//            'data_address.*.postal_code' => 'required',
//            'data_address.*.country' => 'required',
            'data_email.*.email' => 'required|email',
            'data_phone.*.phone' => 'required',
        ],
   ];
}
