<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class BenefitValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'provider_id' => 'required|integer|exists:providers,id',
            'benefit_category_id' => 'required|integer|exists:benefits_categories,id',
            'name' => 'required|max:255',
            'description' => 'required',
            'users.*' => 'required|exists:users,id',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'provider_id' => 'required|integer|exists:providers,id',
            'benefit_category_id' => 'required|integer|exists:benefits_categories,id',
            'name' => 'required|max:255',
            'description' => 'required',
            'users.*' => 'exists:users,id',
        ],
   ];
}
