<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class CompanyValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'trading_name' => 'required|string|max:255',
            'company_name' => 'string|max:255',
            'national_register_of_legal_entities' => 'required|unique:companies,national_register_of_legal_entities',
            'state_registration' => 'required_if:state_registration_exempted,0|unique:companies,national_register_of_legal_entities',
            'sector_id' => 'integer|exists:sectors,id',
            'sector_name' => 'required|string|max:255',
            'tax_framework_id' => 'required_if:tax_framework_nope,0|integer|exists:taxes_frameworks,id',
//            'address' => 'required',
//            'data_address.*.street' => 'required',
//            'data_address.*.number' => 'required',
//            'data_address.*.neighborhood' => 'required',
//            'data_address.*.state' => 'required',
//            'data_address.*.city' => 'required',
//            'data_address.*.postal_code' => 'required',
//            'data_address.*.country' => 'required',
            'data_email.*.main' => 'required|boolean',
            'data_email.*.email' => 'required|email',
            'data_phone.*.main' => 'required',
            'data_phone.*.phone' => 'required',
        ],

        ValidatorInterface::RULE_UPDATE => [
            'trading_name' => 'required|string|max:255',
            'company_name' => 'string|max:255',
            'national_register_of_legal_entities' => 'required|unique:companies,national_register_of_legal_entities',
            'state_registration' => 'required_if:state_registration_exempted,0|unique:companies,national_register_of_legal_entities',
            'sector_id' => 'integer|exists:sectors,id',
            'sector_name' => 'required|string|max:255',
            'tax_framework_id' => 'integer|exists:taxes_frameworks,id',
//            'address' => 'required',
//            'data_address.*.street' => 'required',
//            'data_address.*.number' => 'required',
//            'data_address.*.neighborhood' => 'required',
//            'data_address.*.state' => 'required',
//            'data_address.*.city' => 'required',
//            'data_address.*.postal_code' => 'required',
//            'data_address.*.country' => 'required',
            'data_email.*.main' => 'required|boolean',
            'data_email.*.email' => 'required|email',
            'data_phone.*.main' => 'required',
            'data_phone.*.phone' => 'required',
        ],
    ];

}
