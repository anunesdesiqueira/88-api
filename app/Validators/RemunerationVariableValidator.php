<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class RemunerationVariableValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|max:255',
        ],

        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|max:255',
        ],
   ];
}
