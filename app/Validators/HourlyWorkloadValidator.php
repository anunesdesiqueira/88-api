<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class HourlyWorkloadValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|max:255',
            'data_week.*.day' => 'required|max:255',
            'users.*' => 'required|exists:users,id',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|max:255',
            'data_week.*.day' => 'required|max:255',
            'users.*' => 'exists:users,id',
        ],
   ];
}
