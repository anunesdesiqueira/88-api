<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class UserProfileValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|string|max:255',
            'gender' => 'required|in:male,female',
            'date_of_birth' => 'required|date',
            'identity_document' => 'required_if:identity_document_nope,0|unique:users_profiles,identity_document',
            'identity_document_state' => 'required_if:identity_document_nope,0',
            'identity_document_dispatcher_organ' => 'required_if:identity_document_nope,0',
            'identity_document_dispatcher_date' => 'required_if:identity_document_nope,0|date',
            'register_individual' => 'required|unique:users_profiles,register_individual',
            'voter_id_card' => 'required_if:voter_id_card_nope,0|unique:users_profiles,voter_id_card',
            'driving_licence' => 'required_if:driving_licence_nope,0|unique:users_profiles,driving_licence',
            'driving_licence_category' => 'required_if:driving_licence_nope,0',
            'driving_licence_dispatcher_date' => 'required_if:driving_licence_nope,0|date',
            'driving_licence_validate_date' => 'required_if:driving_licence_nope,0|date',
            'work_record_booklet' => 'required_if:work_record_booklet_nope,0|unique:users_profiles,work_record_booklet',
            'work_record_booklet_series' => 'required_if:work_record_booklet_nope,0',
            'work_record_booklet_dispatcher_state' => 'required_if:work_record_booklet_nope,0',
            'social_integration_program' => 'required_if:social_integration_program_nope,0|unique:users_profiles,social_integration_program',
            'professional_identity.*.type' => 'required_if:professional_identity_nope,0',
            'professional_identity.*.number' => 'required_if:professional_identity_nope,0',
            'address' => 'required',
//            'data_address.*.street' => 'required',
//            'data_address.*.number' => 'required',
//            'data_address.*.neighborhood' => 'required',
//            'data_address.*.state' => 'required',
//            'data_address.*.city' => 'required',
//            'data_address.*.postal_code' => 'required',
//            'data_address.*.country' => 'required',
//            'data_email.*.main' => 'required|boolean',
//            'data_email.*.email' => 'required|email',
//            'data_phone.*.main' => 'required',
//            'data_phone.*.phone' => 'required',
//            'data_phone_emergency.*.type' => 'required',
//            'data_phone_emergency.*.name' => 'required',
//            'data_phone_emergency.*.phone' => 'required',
//            'about_me' => 'required',
        ],

        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|string|max:255',
            'gender' => 'required|in:male,female',
            'date_of_birth' => 'required|date',
            'identity_document' => 'required_if:identity_document_nope,0|unique:users_profiles,identity_document',
            'identity_document_state' => 'required_if:identity_document_nope,0',
            'identity_document_dispatcher_organ' => 'required_if:identity_document_nope,0',
            'identity_document_dispatcher_date' => 'required_if:identity_document_nope,0|date',
            'register_individual' => 'required|unique:users_profiles,register_individual',
            'voter_id_card' => 'required_if:voter_id_card_nope,0|unique:users_profiles,voter_id_card',
            'driving_licence' => 'required_if:driving_licence_nope,0|unique:users_profiles,driving_licence',
            'driving_licence_category' => 'required_if:driving_licence_nope,0',
            'driving_licence_dispatcher_date' => 'required_if:driving_licence_nope,0|date',
            'driving_licence_validate_date' => 'required_if:driving_licence_nope,0|date',
            'work_record_booklet' => 'required_if:work_record_booklet_nope,0|unique:users_profiles,work_record_booklet',
            'work_record_booklet_series' => 'required_if:work_record_booklet_nope,0',
            'work_record_booklet_dispatcher_state' => 'required_if:work_record_booklet_nope,0',
            'social_integration_program' => 'required_if:social_integration_program_nope,0|unique:users_profiles,social_integration_program',
            'professional_identity.*.type' => 'required_if:professional_identity_nope,0',
            'professional_identity.*.number' => 'required_if:professional_identity_nope,0',
            'address' => 'required',
//            'data_address.*.street' => 'required',
//            'data_address.*.number' => 'required',
//            'data_address.*.neighborhood' => 'required',
//            'data_address.*.state' => 'required',
//            'data_address.*.city' => 'required',
//            'data_address.*.postal_code' => 'required',
//            'data_address.*.country' => 'required',
//            'data_email.*.main' => 'required|boolean',
//            'data_email.*.email' => 'required|email',
//            'data_phone.*.main' => 'required',
//            'data_phone.*.phone' => 'required',
//            'data_phone_emergency.*.type' => 'required',
//            'data_phone_emergency.*.name' => 'required',
//            'data_phone_emergency.*.phone' => 'required',
//            'about_me' => 'required',
        ],
    ];


}