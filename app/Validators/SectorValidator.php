<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class SectorValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|max:255',
            'slug' => 'required|unique:sectors,slug|max:255',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|max:255',
            'slug' => 'required|unique:sectors,slug|max:255',
        ],
   ];
}
