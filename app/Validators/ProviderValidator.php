<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class ProviderValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'provider_category_id' => 'required|integer|exists:providers_categories,id',
            'name' => 'required|max:255',
            'contact_name' => 'required|max:255',
            'data_phone.*.phone' => 'required',
            'data_email.*.email' => 'required|email',
            'address' => 'required',
            'data_address.*.street' => 'required',
            'data_address.*.number' => 'required',
            'data_address.*.neighborhood' => 'required',
            'data_address.*.state' => 'required',
            'data_address.*.city' => 'required',
//            'data_address.*.postal_code' => 'required',
            'data_address.*.country' => 'required',
            'benefit' => 'required'
        ],

        ValidatorInterface::RULE_UPDATE => [
            'provider_category_id' => 'required|integer|exists:providers_categories,id',
            'name' => 'required|max:255',
            'contact_name' => 'required|max:255',
            'data_phone.*.phone' => 'required',
            'data_email.*.email' => 'required|email',
            'address' => 'required',
            'data_address.*.street' => 'required',
            'data_address.*.number' => 'required',
            'data_address.*.neighborhood' => 'required',
            'data_address.*.state' => 'required',
            'data_address.*.city' => 'required',
//            'data_address.*.postal_code' => 'required',
            'data_address.*.country' => 'required',
            'benefit' => 'required'
        ],
   ];
}
