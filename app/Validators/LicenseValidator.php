<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class LicenseValidator extends Validator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'user_id' => 'required|integer|exists:users,id',
            'status' => 'required|in:requested,approved,refused',
            'starting' => 'required|date',
            'ending' => 'required|date|after:starting',
            'type' => 'required',
            'icd' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'status' => 'required|in:requested,approved,refused',
            'starting' => 'required|date',
            'ending' => 'required|date|after:starting',
            'type' => 'required',
            'icd' => 'required',
        ],
   ];
}
