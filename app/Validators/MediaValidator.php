<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class MediaValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'slug' => 'required',
            'file' => 'required',

        ],
        ValidatorInterface::RULE_UPDATE => [
            'file' => 'required',
        ],
    ];
}
