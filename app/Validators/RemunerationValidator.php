<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class RemunerationValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [

        ],
        ValidatorInterface::RULE_UPDATE => [
            'variable.remuneration_variable_id' => 'required_if:variable_register,1|integer|exists:remunerations_variables,id',
            'variable.value_or_amount' => 'required_if:variable_register,1',
            'net_salary' => 'required_if:payslip_register,1',
            'file_id' => 'required_if:payslip_register,1|integer|exists:medias,id',
        ],
   ];
}
