<?php

namespace OitentaOito\Validators;

use Prettus\Validator\LaravelValidator;

class Validator extends LaravelValidator
{
    /**
     * @var
     */
    protected $id = null;


    /**
     * @param $id
     * @return $this|void
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @var
     */
    protected $where = null;


    /**
     * @param $primary
     * @param $field
     * @param $param
     * @return $this
     */
    public function setWhere($primary, $field, $param)
    {
        $this->where = $primary . ',' . $field . ',' . $param;
        return $this;
    }


    /**
     * Parser Validation Rules
     *
     * @param $rules
     * @param null $id
     * @return array
     */
    protected function parserValidationRules($rules, $id = null)
    {
        if($id === null && $this->where === null)
        {
            return $rules;
        }

        array_walk($rules, function(&$rules, $field) use ($id)
        {
            if(!is_array($rules))
            {
                $rules = explode("|", $rules);
            }

            foreach($rules as $ruleIdx => $rule)
            {
                // get name and parameters
                @list($name, $params) = array_pad(explode(":", $rule), 2, null);

                // only do someting for the unique rule
                if(strtolower($name) != "unique") {
                    continue; // continue in foreach loop, nothing left to do here
                }

                $p = array_map("trim", explode(",", $params));

                // set field name to rules key ($field) (laravel convention)
                if(!isset($p[1])) {
                    $p[1] = $field;
                }

                // set 3rd parameter to id given to getValidationRules()
                $p[2] = ($id === null? 'null' : $id);

                if($this->where !== null) {
                    $p[3] = $this->where;
                }

                $params = implode(",", $p);
                $rules[$ruleIdx] = $name.":".$params;
            }
        });

        return $rules;
    }
}