<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class DocumentValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'user_id' => 'integer|exists:users,id',
            'slug' => 'required',
            'name' => 'required|max:255',
            'folder' => 'required|in:company,team,personal',
            'access' => 'required|in:public,private',
            'file' => 'required',
        ],

        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|max:255',
            'folder' => 'required|in:company,team,personal',
            'access' => 'required|in:public,private',
        ],
   ];
}
