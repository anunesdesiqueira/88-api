<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class TaxFrameworkValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|max:255',
            'slug' => 'required|unique:taxes_frameworks,slug|max:255',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|max:255',
            'slug' => 'required|unique:taxes_frameworks,slug|max:255',
        ],
   ];
}
