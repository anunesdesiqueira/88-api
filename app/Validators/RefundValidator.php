<?php

namespace OitentaOito\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;

class RefundValidator extends Validator
{
    /**
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'user_id' => 'required|integer|exists:users,id',
            'type_expenditure_id' => 'required|integer|exists:types_expenditures,id',
            'status' => 'required|in:requested,approved,refused,paid',
            'date' => 'required|date',
            'value' => 'required',
            'description' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'type_expenditure_id' => 'required|integer|exists:types_expenditures,id',
            'status' => 'required|in:requested,approved,refused,paid',
            'date' => 'required|date',
            'value' => 'required',
            'description' => 'required',
        ],
   ];
}
