<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\CityPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\CityRepository;
use OitentaOito\Entities\City;

/**
 * Class CityRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class CityRepositoryEloquent extends BaseRepository implements CityRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return City::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return CityPresenter::class;
    }
}
