<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StateRepository
 * @package namespace OitentaOito\Repositories;
 */
interface CompanyRepository extends RepositoryInterface
{
    //
}
