<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RemunerationVariableRepository
 * @package namespace OitentaOito\Repositories;
 */
interface RemunerationVariableRepository extends RepositoryInterface
{
    //
}
