<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\SectorPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\SectorRepository;
use OitentaOito\Entities\Sector;

/**
 * Class SectorRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class SectorRepositoryEloquent extends BaseRepository implements SectorRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Sector::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return SectorPresenter::class;
    }
}
