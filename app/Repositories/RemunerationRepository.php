<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RemunerationRepository
 * @package namespace OitentaOito\Repositories;
 */
interface RemunerationRepository extends RepositoryInterface
{
    //
}
