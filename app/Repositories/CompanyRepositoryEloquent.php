<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\CompanyPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\CompanyRepository;
use OitentaOito\Entities\Company;

/**
 * Class SectorRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class CompanyRepositoryEloquent extends BaseRepository implements CompanyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Company::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return CompanyPresenter::class;
    }
}
