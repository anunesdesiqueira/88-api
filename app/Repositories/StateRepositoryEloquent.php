<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\StatePresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\StateRepository;
use OitentaOito\Entities\State;

/**
 * Class StateRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class StateRepositoryEloquent extends BaseRepository implements StateRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return State::class;
    }
    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return StatePresenter::class;
    }
}
