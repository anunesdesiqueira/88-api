<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\ProviderCategoryPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\ProviderCategoryRepository;
use OitentaOito\Entities\ProviderCategory;

/**
 * Class ProviderCategoryRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class ProviderCategoryRepositoryEloquent extends BaseRepository implements ProviderCategoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProviderCategory::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return ProviderCategoryPresenter::class;
    }
}
