<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface HourlyWorkloadRepository
 * @package namespace OitentaOito\Repositories;
 */
interface HourlyWorkloadRepository extends RepositoryInterface
{
    //
}
