<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SubsidiaryRepository
 * @package namespace OitentaOito\Repositories;
 */
interface SubsidiaryRepository extends RepositoryInterface
{
    //
}
