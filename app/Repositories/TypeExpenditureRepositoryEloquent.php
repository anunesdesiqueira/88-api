<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\TypeExpenditurePresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\TypeExpenditureRepository;
use OitentaOito\Entities\TypeExpenditure;

/**
 * Class TypeExpenditureRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class TypeExpenditureRepositoryEloquent extends BaseRepository implements TypeExpenditureRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TypeExpenditure::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return TypeExpenditurePresenter::class;
    }
}
