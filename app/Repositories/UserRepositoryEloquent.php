<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\UserRepository;
use OitentaOito\Entities\User;
use OitentaOito\Presenters\UserPresenter;

/**
 * Class UserRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email' => 'like'
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return UserPresenter::class;
    }
}