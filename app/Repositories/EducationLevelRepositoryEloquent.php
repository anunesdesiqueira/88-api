<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\EducationLevelRepository;
use OitentaOito\Entities\EducationLevel;
use OitentaOito\Presenters\EducationLevelPresenter;

/**
 * Class EducationLevelRepositoryEloquent
 * @package OitentaOito\Repositories
 */
class EducationLevelRepositoryEloquent extends BaseRepository implements EducationLevelRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EducationLevel::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return EducationLevelPresenter::class;
    }
}
