<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DocumentRepository
 * @package namespace OitentaOito\Repositories;
 */
interface DocumentRepository extends RepositoryInterface
{
    //
}
