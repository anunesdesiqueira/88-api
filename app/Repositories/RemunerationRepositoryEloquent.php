<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\RemunerationPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\RemunerationRepository;
use OitentaOito\Entities\Remuneration;

/**
 * Class RemunerationRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class RemunerationRepositoryEloquent extends BaseRepository implements RemunerationRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Remuneration::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return RemunerationPresenter::class;
    }
}
