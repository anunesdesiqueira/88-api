<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AbsenceRepository
 * @package namespace OitentaOito\Repositories;
 */
interface AbsenceRepository extends RepositoryInterface
{
    //
}
