<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InvitationRepository
 * @package namespace OitentaOito\Repositories;
 */
interface InvitationRepository extends RepositoryInterface
{
    //
}
