<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoleUserRepository
 * @package namespace OitentaOito\Repositories;
 */
interface RoleUserRepository extends RepositoryInterface
{
    //
}
