<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\RoleUserRepository;
use OitentaOito\Entities\RoleUser;
use OitentaOito\Validators\RoleUserValidator;

/**
 * Class RoleUserRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class RoleUserRepositoryEloquent extends BaseRepository implements RoleUserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RoleUser::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
