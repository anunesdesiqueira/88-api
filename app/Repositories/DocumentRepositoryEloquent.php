<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\DocumentPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\DocumentRepository;
use OitentaOito\Entities\Document;

/**
 * Class DocumentRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class DocumentRepositoryEloquent extends BaseRepository implements DocumentRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'folder',
        'name' => 'like',
        'user.email' => 'like',
        'user.profile.name' => 'like',
    ];


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Document::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return DocumentPresenter::class;
    }
}
