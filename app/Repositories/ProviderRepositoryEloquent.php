<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\ProviderPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\ProviderRepository;
use OitentaOito\Entities\Provider;

/**
 * Class ProviderRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class ProviderRepositoryEloquent extends BaseRepository implements ProviderRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'company_id',
        'provider_category_id',
        'name' => 'like'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Provider::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return ProviderPresenter::class;
    }
}
