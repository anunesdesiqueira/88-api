<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TypeExpenditureRepository
 * @package namespace OitentaOito\Repositories;
 */
interface TypeExpenditureRepository extends RepositoryInterface
{
    //
}
