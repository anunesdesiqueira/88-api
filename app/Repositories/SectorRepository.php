<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SectorRepository
 * @package namespace OitentaOito\Repositories;
 */
interface SectorRepository extends RepositoryInterface
{
    //
}
