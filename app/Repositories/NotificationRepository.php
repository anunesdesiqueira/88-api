<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NotificationRepository
 * @package namespace OitentaOito\Repositories;
 */
interface NotificationRepository extends RepositoryInterface
{
    //
}
