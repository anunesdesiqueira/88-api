<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\NotificationPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\NotificationRepository;
use OitentaOito\Entities\Notification;

/**
 * Class NotificationRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class NotificationRepositoryEloquent extends BaseRepository implements NotificationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Notification::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return NotificationPresenter::class;
    }
}
