<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\HourlyWorkloadPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\HourlyWorkloadRepository;
use OitentaOito\Entities\HourlyWorkload;

/**
 * Class HourlyWorkloadRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class HourlyWorkloadRepositoryEloquent extends BaseRepository implements HourlyWorkloadRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return HourlyWorkload::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return HourlyWorkloadPresenter::class;
    }
}
