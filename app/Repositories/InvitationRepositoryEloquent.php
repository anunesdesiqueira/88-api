<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\InvitationPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\InvitationRepository;
use OitentaOito\Entities\Invitation;

/**
 * Class InvitationRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class InvitationRepositoryEloquent extends BaseRepository implements InvitationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Invitation::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return InvitationPresenter::class;
    }
}
