<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\UserProfilePresenter;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\UserProfileRepository;
use OitentaOito\Entities\UserProfile;

/**
 * Class UserProfileRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class UserProfileRepositoryEloquent extends BaseCustomRepository implements UserProfileRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserProfile::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return UserProfilePresenter::class;
    }
}
