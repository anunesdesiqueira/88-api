<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CalendarRepository
 * @package namespace OitentaOito\Repositories;
 */
interface CalendarRepository extends RepositoryInterface
{
    //
}
