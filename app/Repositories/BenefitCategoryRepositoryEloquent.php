<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\BenefitCategoryPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\BenefitCategoryRepository;
use OitentaOito\Entities\BenefitCategory;

/**
 * Class ProviderCategoryRepositoryEloquent
 * @package OitentaOito\Repositories
 */
class BenefitCategoryRepositoryEloquent extends BaseRepository implements BenefitCategoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BenefitCategory::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return BenefitCategoryPresenter::class;
    }
}
