<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\AbsencePresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\AbsenceRepository;
use OitentaOito\Entities\Absence;

/**
 * Class AbsenceRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class AbsenceRepositoryEloquent extends BaseRepository implements AbsenceRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'status',
        'starting' => '>=',
        'ending' => '<=',
        'justified',
    ];


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Absence::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return AbsencePresenter::class;
    }
}
