<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\SubsidiaryPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\SubsidiaryRepository;
use OitentaOito\Entities\Company;

/**
 * Class SubsidiaryRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class SubsidiaryRepositoryEloquent extends BaseRepository implements SubsidiaryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Company::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return SubsidiaryPresenter::class;
    }
}
