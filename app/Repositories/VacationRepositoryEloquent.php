<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\VacationPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\VacationRepository;
use OitentaOito\Entities\Vacation;

/**
 * Class VacationRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class VacationRepositoryEloquent extends BaseRepository implements VacationRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'department_id',
        'name',
        'status',
        'starting' => '>=',
        'ending' => '<=',
    ];


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Vacation::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return VacationPresenter::class;
    }
}
