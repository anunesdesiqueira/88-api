<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\CalendarPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\CalendarRepository;
use OitentaOito\Entities\Calendar;

/**
 * Class CalendarRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class CalendarRepositoryEloquent extends BaseRepository implements CalendarRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Calendar::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return CalendarPresenter::class;
    }
}
