<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\MediaPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\MediaRepository;
use OitentaOito\Entities\Media;

/**
 * Class MediaRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class MediaRepositoryEloquent extends BaseRepository implements MediaRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'reference',
        'slug',
    ];


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Media::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return MediaPresenter::class;
    }
}
