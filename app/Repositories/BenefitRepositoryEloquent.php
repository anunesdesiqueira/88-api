<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\BenefitPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\BenefitRepository;
use OitentaOito\Entities\Benefit;

/**
 * Class BenefitRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class BenefitRepositoryEloquent extends BaseRepository implements BenefitRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'company_id',
        'benefit_category_id',
        'provider_id',
        'name' => 'like'
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Benefit::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return BenefitPresenter::class;
    }
}
