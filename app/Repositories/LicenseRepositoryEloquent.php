<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\LicensePresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\LicenseRepository;
use OitentaOito\Entities\License;

/**
 * Class LicenseRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class LicenseRepositoryEloquent extends BaseRepository implements LicenseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'status',
        'starting' => '>=',
        'ending' => '<=',
    ];


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return License::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return LicensePresenter::class;
    }
}
