<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CityRepository
 * @package namespace OitentaOito\Repositories;
 */
interface CityRepository extends RepositoryInterface
{
    //
}
