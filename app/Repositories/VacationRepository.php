<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VacationRepository
 * @package namespace OitentaOito\Repositories;
 */
interface VacationRepository extends RepositoryInterface
{
    //
}
