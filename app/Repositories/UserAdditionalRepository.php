<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserAdditionalRepository
 * @package OitentaOito\Repositories
 */
interface UserAdditionalRepository extends RepositoryInterface
{
    //
}
