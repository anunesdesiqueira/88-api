<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserProfileRepository
 * @package namespace OitentaOito\Repositories;
 */
interface UserProfileRepository extends RepositoryInterface
{
    //
}
