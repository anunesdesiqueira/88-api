<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\RefundPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\RefundRepository;
use OitentaOito\Entities\Refund;

/**
 * Class RefundRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class RefundRepositoryEloquent extends BaseRepository implements RefundRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'type_expenditure_id',
        'status',
        'date' => '>=',
    ];


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Refund::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return RefundPresenter::class;
    }
}
