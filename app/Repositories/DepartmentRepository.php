<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DepartmentRepository
 * @package namespace OitentaOito\Repositories;
 */
interface DepartmentRepository extends RepositoryInterface
{
    //
}
