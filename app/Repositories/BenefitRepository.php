<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BenefitRepository
 * @package namespace OitentaOito\Repositories;
 */
interface BenefitRepository extends RepositoryInterface
{
    //
}
