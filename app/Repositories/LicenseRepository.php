<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LicenseRepository
 * @package namespace OitentaOito\Repositories;
 */
interface LicenseRepository extends RepositoryInterface
{
    //
}
