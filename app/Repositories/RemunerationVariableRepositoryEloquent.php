<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\RemunerationVariablePresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\remunerationVariableRepository;
use OitentaOito\Entities\RemunerationVariable;

/**
 * Class RemunerationVariableRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class RemunerationVariableRepositoryEloquent extends BaseRepository implements RemunerationVariableRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RemunerationVariable::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return RemunerationVariablePresenter::class;
    }
}
