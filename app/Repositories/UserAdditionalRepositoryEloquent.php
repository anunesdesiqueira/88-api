<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\UserAdditionalPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\UserAdditionalRepository;
use OitentaOito\Entities\UserAdditional;

/**
 * Class UserAdditionalRepositoryEloquent
 * @package OitentaOito\Repositories
 */
class UserAdditionalRepositoryEloquent extends BaseCustomRepository implements UserAdditionalRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserAdditional::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return UserAdditionalPresenter::class;
    }
}
