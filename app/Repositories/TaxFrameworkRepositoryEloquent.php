<?php

namespace OitentaOito\Repositories;

use OitentaOito\Presenters\TaxFrameworkPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\TaxFrameworkRepository;
use OitentaOito\Entities\TaxFramework;

/**
 * Class SectorRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class TaxFrameworkRepositoryEloquent extends BaseRepository implements TaxFrameworkRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TaxFramework::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return TaxFrameworkPresenter::class;
    }
}
