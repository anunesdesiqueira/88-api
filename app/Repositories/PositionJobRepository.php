<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OfficeRepository
 * @package namespace OitentaOito\Repositories;
 */
interface PositionJobRepository extends RepositoryInterface
{
    //
}
