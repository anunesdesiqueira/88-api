<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BenefitCategoryRepository
 * @package OitentaOito\Repositories
 */
interface BenefitCategoryRepository extends RepositoryInterface
{
    //
}
