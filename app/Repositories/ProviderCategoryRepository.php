<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProviderCategoryRepository
 * @package namespace OitentaOito\Repositories;
 */
interface ProviderCategoryRepository extends RepositoryInterface
{
    //
}
