<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MediaRepository
 * @package namespace OitentaOito\Repositories;
 */
interface MediaRepository extends RepositoryInterface
{
    //
}
