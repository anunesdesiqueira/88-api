<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProviderRepository
 * @package namespace OitentaOito\Repositories;
 */
interface ProviderRepository extends RepositoryInterface
{
    //
}
