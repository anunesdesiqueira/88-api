<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RefundRepository
 * @package namespace OitentaOito\Repositories;
 */
interface RefundRepository extends RepositoryInterface
{
    //
}
