<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EducationLevelRepository
 * @package OitentaOito\Repositories
 */
interface EducationLevelRepository extends RepositoryInterface
{
    //
}
