<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace OitentaOito\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}