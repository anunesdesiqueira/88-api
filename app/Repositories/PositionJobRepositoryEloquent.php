<?php

namespace OitentaOito\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use OitentaOito\Repositories\PositionJobRepository;
use OitentaOito\Entities\PositionJob;
use OitentaOito\Presenters\PositionJobPresenter;

/**
 * Class OfficeRepositoryEloquent
 * @package namespace OitentaOito\Repositories;
 */
class PositionJobRepositoryEloquent extends BaseRepository implements PositionJobRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PositionJob::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Transform
     */
    public function presenter()
    {
        return PositionJobPresenter::class;
    }
}
