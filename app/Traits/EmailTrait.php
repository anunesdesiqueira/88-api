<?php

namespace OitentaOito\Traits;


trait EmailTrait
{
    /**
     * @param $value
     */
    public function setDataEmailAttribute($value) {
        $this->attributes['data_email'] = json_encode($value);
    }


    /**
     * @param $value
     * @return mixed
     */
    public function getDataEmailAttribute($value) {
        return json_decode($value);
    }
}