<?php

namespace OitentaOito\Traits;

trait CreatedUserIdTrait
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdUserId()
    {
        return $this->belongsTo('OitentaOito\Entities\User', 'created_user_id');
    }
}