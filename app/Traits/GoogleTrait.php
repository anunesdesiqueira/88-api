<?php

namespace OitentaOito\Traits;

use Google_Client;

trait GoogleTrait
{
    protected function getClient()
    {
        $client = new Google_Client();
        $client->setAuthConfig(
            storage_path('credentials/client_secret_957595828821-7bkimvs1amk0i3aeo82357c9nhs90lrp.apps.googleusercontent.com.json')
        );

        return $client;

    }

    public function getUserForCode($code)
    {
        $client = $this->getClient();
        $client->setScopes(['email','profile']);
        $token = $client->fetchAccessTokenWithAuthCode($code);

        $response = $client->getHttpClient()->get('https://www.googleapis.com/oauth2/v1/tokeninfo?', [
            'query' => [
                'access_token' => $token['access_token'],
            ],
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        return json_decode($response->getBody(), true);
    }
}