<?php

namespace OitentaOito\Traits;

use Carbon\Carbon;

trait PeriodTrait
{
    /**
     * @param $value
     */
    public function setStartingAttribute($value) {
        $this->attributes['starting'] = Carbon::parse($value)->format('Y-m-d');
    }


    /**
     * @param $value
     */
    public function setEndingAttribute($value) {
        $this->attributes['ending'] = Carbon::parse($value)->format('Y-m-d');
    }
}