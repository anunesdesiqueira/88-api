<?php

namespace OitentaOito\Traits;

trait AddressTrait
{
    /**
     * @param $value
     */
    public function setDataAddressAttribute($value) {
        $this->attributes['data_address'] = json_encode($value);
    }


    /**
     * @param $value
     * @return mixed
     */
    public function getDataAddressAttribute($value) {
        return json_decode($value);
    }
}