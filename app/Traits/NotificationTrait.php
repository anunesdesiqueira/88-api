<?php

namespace OitentaOito\Traits;

use Carbon\Carbon;
use OitentaOito\Entities\Notification;

/**
 * Class NotificationTrait
 * @package OitentaOito\Traits
 */
trait NotificationTrait
{
    /**
     * @param $userId
     * @param $companyId
     * @param $slug
     * @param $message
     * @return Notification
     */
    protected function notification($userId, $companyId, $slug, $message)
    {
        $data = [
            'user_id' => $userId,
            'company_id' => $companyId,
            'notification_at' => Carbon::now(),
            'slug' => $slug,
            'message' => trans($message),
        ];

        $notification = Notification::create($data);
        return $notification;
    }
}