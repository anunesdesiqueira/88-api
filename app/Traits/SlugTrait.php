<?php

namespace OitentaOito\Traits;

use Illuminate\Support\Str;

trait SlugTrait
{
    /**
     * @param $value
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value);
    }
}