<?php

namespace OitentaOito\Traits;

trait PhoneTrait
{
    /**
     * @param $value
     */
    public function setDataPhoneAttribute($value) {
        $this->attributes['data_phone'] = json_encode($value);
    }


    /**
     * @param $value
     * @return mixed
     */
    public function getDataPhoneAttribute($value) {
        return json_decode($value);
    }


    /**
     * @param $value
     */
    public function setDataPhoneEmergencyAttribute($value) {
        $this->attributes['data_phone_emergency'] = json_encode($value);
    }


    /**
     * @param $value
     * @return mixed
     */
    public function getDataPhoneEmergencyAttribute($value) {
        return json_decode($value);
    }
}