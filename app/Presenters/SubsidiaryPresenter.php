<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\SubsidiaryTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class SubsidiaryPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class SubsidiaryPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new SubsidiaryTransformer();
    }
}
