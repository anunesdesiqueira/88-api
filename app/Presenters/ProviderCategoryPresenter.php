<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\ProviderCategoryTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ProviderCategoryPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class ProviderCategoryPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ProviderCategoryTransformer();
    }
}
