<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\TaxFrameworkTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class SectorPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class TaxFrameworkPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TaxFrameworkTransformer();
    }
}
