<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\BenefitCategoryTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BenefitCategoryPresenter
 * @package OitentaOito\Presenters
 */
class BenefitCategoryPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BenefitCategoryTransformer();
    }
}
