<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\RemunerationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RemunerationPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class RemunerationPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RemunerationTransformer();
    }
}
