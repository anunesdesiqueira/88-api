<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\InvitationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class InvitationPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class InvitationPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new InvitationTransformer();
    }
}
