<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\LicenseTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class LicensePresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class LicensePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new LicenseTransformer();
    }
}
