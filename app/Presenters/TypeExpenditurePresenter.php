<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\TypeExpenditureTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TypeExpenditurePresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class TypeExpenditurePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TypeExpenditureTransformer();
    }
}
