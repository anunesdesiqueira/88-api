<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\RefundTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RefundPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class RefundPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RefundTransformer();
    }
}
