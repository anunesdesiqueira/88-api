<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\BenefitTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BenefitPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class BenefitPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BenefitTransformer();
    }
}
