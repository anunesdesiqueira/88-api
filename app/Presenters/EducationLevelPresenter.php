<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\EducationLevelTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EducationLevelPresenter
 * @package OitentaOito\Presenters
 */
class EducationLevelPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EducationLevelTransformer();
    }
}
