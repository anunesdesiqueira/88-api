<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\RemunerationVariableTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RemunerationVariablePresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class RemunerationVariablePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RemunerationVariableTransformer();
    }
}
