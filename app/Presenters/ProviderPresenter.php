<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\ProviderTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ProviderPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class ProviderPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ProviderTransformer();
    }
}
