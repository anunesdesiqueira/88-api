<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\UserProfileTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class UserProfilePresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class UserProfilePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new UserProfileTransformer();
    }
}
