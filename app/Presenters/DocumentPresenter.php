<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\DocumentTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class DocumentPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class DocumentPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new DocumentTransformer();
    }
}
