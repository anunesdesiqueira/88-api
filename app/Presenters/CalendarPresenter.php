<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\CalendarTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CalendarPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class CalendarPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CalendarTransformer();
    }
}
