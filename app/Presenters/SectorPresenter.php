<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\SectorTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class SectorPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class SectorPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new SectorTransformer();
    }
}
