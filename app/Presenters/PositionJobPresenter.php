<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\PositionJobTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OfficePresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class PositionJobPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PositionJobTransformer();
    }
}
