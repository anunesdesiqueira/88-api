<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\MediaTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MediaPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class MediaPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MediaTransformer();
    }
}
