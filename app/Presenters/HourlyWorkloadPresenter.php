<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\HourlyWorkloadTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class HourlyWorkloadPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class HourlyWorkloadPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new HourlyWorkloadTransformer();
    }
}
