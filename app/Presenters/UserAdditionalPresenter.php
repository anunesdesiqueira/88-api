<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\UserAdditionalTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class UserAdditionalPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class UserAdditionalPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new UserAdditionalTransformer();
    }
}
