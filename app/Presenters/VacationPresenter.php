<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\VacationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class VacationPresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class VacationPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new VacationTransformer();
    }
}
