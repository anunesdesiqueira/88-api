<?php

namespace OitentaOito\Presenters;

use OitentaOito\Transformers\AbsenceTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AbsencePresenter
 *
 * @package namespace OitentaOito\Presenters;
 */
class AbsencePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AbsenceTransformer();
    }
}
