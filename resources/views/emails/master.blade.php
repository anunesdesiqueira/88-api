<!DOCTYPE html>
<head>
    <title>Oitentaeoito - Bem vindo</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
    <style type="text/css">
        @font-face{
            font-family:'Open Sans';
            font-style:normal;
            font-weight:400;
            src:local('Open Sans'),
            local('OpenSans'),
            url('http://fonts.gstatic.com/s/opensans/v10/cJZKeOuBrn4kERxqtaUH3bO3LdcAZYWl9Si6vvxL-qU.woff') format('woff');
        }
        .emailContent{
            font-family:'Open Sans', 'Helvetica Neue', Helvetica, sans-serif;
        }

    </style>
</head>
<body style="background-color:#fff; font-family: 'Open Sans', sans-serif; font-size: 16px; text-align: center;">
    @yield('content')
</body>