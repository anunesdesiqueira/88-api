@extends('emails.master')
<table border="0" cellpadding="0" cellspacing="0" style="font-size: 12px !important;">
    <tr>
        <td>
            To reset your password, complete this form:
            {{ URL::to('password/reset', array($token)) }}.
        </td>
    </tr>
</table>