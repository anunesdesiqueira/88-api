@extends('emails.master')
<table id="Table_01" width="600" height="1351" border="0" cellpadding="0" cellspacing="0" style="font-size: 12px !important;">
    <tr>
        <td colspan="13">
            <img src="{{ asset("images/new_user_admin_01.jpg") }}" width="600" height="110" alt=""></td>
    </tr>
    <tr>
        <td colspan="4" rowspan="2">
            <img src="{{ asset("images/new_user_admin_02.jpg") }}" width="241" height="206" alt=""></td>
        <td colspan="5">
            <a href="http://88.app.dbr.ag">
                <img src="{{ asset("images/new_user_admin_07.jpg") }}" width="130" height="114" border="0" alt=""></a></td>
        <td colspan="4" rowspan="2">
            <img src="{{ asset("images/new_user_admin_04.jpg") }}" width="229" height="206" alt=""></td>
    </tr>
    <tr>
        <td colspan="5">
            <img src="{{ asset("images/new_user_admin_05.jpg") }}" width="130" height="92" alt=""></td>
    </tr>
    <tr>
        <td rowspan="4">
            <img src="{{ asset("images/new_user_admin_06.jpg") }}" width="112" height="230" alt=""></td>
        <td width="380" height="73" colspan="10">
            <h2 style="color: #019098; font-weight: bold; font-size: 40px; ">Seja bem-vindo à nova era do RH!</h2>
        </td>
        <td colspan="2" rowspan="4">
            <img src="{{ asset("images/new_user_admin_08.jpg") }}" width="108" height="230" alt=""></td>
    </tr>
    <tr>
        <td colspan="10">
            <img src="{{ asset("images/new_user_admin_09.jpg") }}" width="380" height="38" alt=""></td>
    </tr>
    <tr>
        <td width="380" height="75" colspan="10">

            Agora sua empresa faz parte do 88, o melhor aliado para tornar o seu departamento pessoal ou RH mais estratégico e humano. Facilitamos todas as principais tarefas que tiram sua atenção do que realmente importa: a relação entre você e sua equipe.

            <br/>
            <a href="{{ route('oauth::activation', ['activationCode' => $activationCode]) }}">
                Ative seu usuário agora e vamos transformar.
            </a>

        </td>
    </tr>
    <tr>
        <td colspan="10">
            <img src="{{ asset("images/new_user_admin_11.jpg") }}" width="380" height="44" alt=""></td>
    </tr>
    <tr>
        <td colspan="13">
            <img src="{{ asset("images/new_user_admin_12.jpg") }}" width="600" height="139" alt=""></td>
    </tr>
    <tr>
        <td colspan="13">
            <img src="{{ asset("images/new_user_admin_13.jpg") }}" width="600" height="42" alt=""></td>
    </tr>
    <tr>
        <td rowspan="2">
            <img src="{{ asset("images/new_user_admin_14.jpg") }}" width="112" height="129" alt=""></td>
        <td width="387" height="99" colspan="11">Para começar bem, recomendamos que você insira os principais dados de seus funcionários e empresa. Assim, poderemos te ajudar a controlar e lembrar das principais tarefas que devem ser feitas nos próximos meses :) Para isso, acesse a parte de “equipe” no seu menu inicial ou clique na imagem acima.


        </td>
        <td rowspan="2">
            <img src="{{ asset("images/new_user_admin_16.jpg") }}" width="101" height="129" alt=""></td>
    </tr>
    <tr>
        <td colspan="11">
            <img src="{{ asset("images/new_user_admin_17.jpg") }}" width="387" height="30" alt=""></td>
    </tr>
    <tr>
        <td colspan="13">
            <img src="{{ asset("images/new_user_admin_18.jpg") }}" width="600" height="143" alt=""></td>
    </tr>
    <tr>
        <td colspan="13">
            <img src="{{ asset("images/new_user_admin_19.jpg") }}" width="600" height="34" alt=""></td>
    </tr>
    <tr>
        <td rowspan="4">
            <img src="{{ asset("images/new_user_admin_20.jpg") }}" width="112" height="317" alt=""></td>
        <td width="371" height="91" colspan="9">Além disso, nosso atendimento está disponível para qualquer dúvida que você possa ter. É só mandar um email para a gente :)

            Um abraço da equipe 88,
        </td>
        <td colspan="3" rowspan="4">
            <img src="{{ asset("images/new_user_admin_22.jpg") }}" width="117" height="317" alt=""></td>
    </tr>
    <tr>
        <td colspan="9">
            <img src="{{ asset("images/new_user_admin_23.jpg") }}" width="371" height="74" alt=""></td>
    </tr>
    <tr>
        <td rowspan="2">
            <img src="{{ asset("images/new_user_admin_24.jpg") }}" width="103" height="152" alt=""></td>
        <td>
            <img src="{{ asset("images/new_user_admin_25.jpg") }}" width="16" height="16" alt=""></td>
        <td colspan="2" rowspan="2">
            <img src="{{ asset("images/new_user_admin_26.jpg") }}" width="39" height="152" alt=""></td>
        <td>
            <img src="{{ asset("images/new_user_admin_27.jpg") }}" width="23" height="16" alt=""></td>
        <td rowspan="2">
            <img src="{{ asset("images/new_user_admin_28.jpg") }}" width="43" height="152" alt=""></td>
        <td>
            <img src="{{ asset("images/new_user_admin_29.jpg") }}" width="22" height="16" alt=""></td>
        <td colspan="2" rowspan="2">
            <img src="{{ asset("images/new_user_admin_30.jpg") }}" width="125" height="152" alt=""></td>
    </tr>
    <tr>
        <td>
            <img src="{{ asset("images/new_user_admin_31.jpg") }}" width="16" height="136" alt=""></td>
        <td>
            <img src="{{ asset("images/new_user_admin_32.jpg") }}" width="23" height="136" alt=""></td>
        <td>
            <img src="{{ asset("images/new_user_admin_33.jpg") }}" width="22" height="136" alt=""></td>
    </tr>
    <tr>
        <td>
            <img src="{{ asset("images/spacer.gif") }}" width="112" height="1" alt=""></td>
        <td>
            <img src="{{ asset("images/spacer.gif") }}" width="103" height="1" alt=""></td>
        <td>
            <img src="{{ asset("images/spacer.gif") }}" width="16" height="1" alt=""></td>
        <td>
            <img src="{{ asset("images/spacer.gif") }}" width="10" height="1" alt=""></td>
        <td>
            <img src="{{ asset("images/spacer.gif") }}" width="29" height="1" alt=""></td>
        <td>
            <img src="{{ asset("images/spacer.gif") }}" width="23" height="1" alt=""></td>
        <td>
            <img src="{{ asset("images/spacer.gif") }}" width="43" height="1" alt=""></td>
        <td>
            <img src="{{ asset("images/spacer.gif") }}" width="22" height="1" alt=""></td>
        <td>
            <img src="{{ asset("images/spacer.gif") }}" width="13" height="1" alt=""></td>
        <td>
            <img src="{{ asset("images/spacer.gif") }}" width="112" height="1" alt=""></td>
        <td>
            <img src="{{ asset("images/spacer.gif") }}" width="9" height="1" alt=""></td>
        <td>
            <img src="{{ asset("images/spacer.gif") }}" width="7" height="1" alt=""></td>
        <td>
            <img src="{{ asset("images/spacer.gif") }}" width="101" height="1" alt=""></td>
    </tr>
</table>