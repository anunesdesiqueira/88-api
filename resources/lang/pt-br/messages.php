<?php

    return [

        "errors"      => [
            "general"                 => "Ops! Aconteceu um erro inseperado.",
            "login_required"          => "Informe o seu email.",
            "password_required"       => "Informe sua senha.",
            "user_not_activate"       => "Cadastro inativo.",
            "wrong_password"          => "Senha incorreta.",
            "user_not_found"           => "Usuário não encontrado.",
            "user_suspended"          => "Usuário suspenso",
            "general_login"           => "Não foi possível efetuar o login, entre em contato com o suporte",
            "user_already_activated"  => "Usuário já ativado",
            "general_code"            => "Não foi possível ativar o usuário, entre em contato com o suporte",
            "activation_code_invalid" => "Código de ativação inválido.",
            "reset_code_invalid"      => "Código de troca de senha inválido.",
            "unable_set_password"     => "Não foi possível alterar sua senha",
            "model_not_found"         => "Registro não encontrado.",
            "user_device"             => "Usuário não tem acesso ao sistema por esse dispositivo.",
            "user_exist"              => "Usuário já cadastrado.",
        ],

        'success'     => [
            "user_welcome" => "Bem vindo :name",
            "user_created" => "Cadastro realizado com sucesso. Enviamos uma confirmação de cadastro no e-mail que você informou como login no ato cadastral. Acesse-o e clique no link para ativar o cadastro.",
            "forget_password_request" => "Enviamos um email para você, acesse o link para cadastrar uma nova senha",
            "new_password_set" => "Senha alterada com sucesso.",
            "activation_code_send_again" => "Enviamos novamente um email para ativação do seu usuário",
            "activation_success" => "Usuário ativado com sucesso, faço o login.",
            "destroy_register" => "Registro deletado",
            "logout" => "Bye.",
            "update" => "Registro atualizado com sucesso",
            "created" => "Registro criado com sucesso",
            "deleted" => "Registro excluído com sucesso",
            "media_add" => "Arquivo adicionado com sucesso",
            "email_send" => "Email de ativação enviado.",
            "user_correction" => "Solicitação de correção enviada com sucesso",
        ],


    ];