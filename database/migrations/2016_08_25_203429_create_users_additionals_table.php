<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersAdditionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_additionals', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->integer('department_id')->nullable()->unsigned();
            $table->integer('position_job_id')->nullable()->unsigned();
            $table->integer('education_level_id')->nullable()->unsigned();
            $table->date('admission_date')->nullable();
            $table->decimal('base_salary',15,2)->nullable();
            $table->text('payday')->nullable();
            $table->text('bank_data')->nullable();
            $table->text('special_needs')->nullable();

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('company_id')->references('id')->on('companies')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('department_id')->references('id')->on('departments')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('position_job_id')->references('id')->on('positions_jobs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('education_level_id')->references('id')->on('educations_levels')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_additionals');
    }
}
