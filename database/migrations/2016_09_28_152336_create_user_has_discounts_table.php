<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserHasDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_has_discounts', function(Blueprint $table) {
            $table->integer('benefit_id')->unsigned();
            $table->integer('user_additional_id')->unsigned();
            $table->enum('type', ['percentage', 'currency']);
            $table->string('value');

            $table->foreign('benefit_id')->references('id')->on('benefits')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_additional_id')->references('id')->on('users_additionals')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->primary(['benefit_id', 'user_additional_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_has_discounts');
    }
}
