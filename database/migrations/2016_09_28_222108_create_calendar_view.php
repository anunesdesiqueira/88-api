<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCalendarView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $prefix = DB::getTablePrefix();
        $viewName = DB::getTablePrefix() . 'calendar';

        DB::statement("
            CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW {$viewName} AS 
                (SELECT
                    'commemorative_date' COLLATE utf8_unicode_ci AS `slug`,
                    NULL AS `company_id`,
                    `commemorative_dates`.`name` AS `name` ,
                    `commemorative_dates`.`date` AS `starting_date`,
                    `commemorative_dates`.`date` AS `ending_date`
                FROM
                    `{$prefix}commemorative_dates` AS `commemorative_dates`
                ORDER BY
                    `commemorative_dates`.`date` ASC)
                
                UNION
                
                (SELECT
                    'birthday_date' COLLATE utf8_unicode_ci AS `slug`,
                    `ru`.`company_id` AS `company_id`,
                    CONCAT('Aniversário de ', `up`.`name`) AS `name`,
                    DATE_FORMAT(`up`.`date_of_birth`, '%m-%d') AS `starting_date`,
                    DATE_FORMAT(`up`.`date_of_birth`, '%m-%d') AS `ending_date`
                FROM
                    `{$prefix}users_profiles` AS `up`
                INNER JOIN `{$prefix}role_user` AS `ru` 
                    ON (`ru`.`user_id` = `up`.`user_id`)
                WHERE 
                    `up`.`date_of_birth`IS NOT NULL
                ORDER BY 
                    `up`.`date_of_birth` ASC)
                
                UNION
                
                (SELECT
                    'vacation_date' COLLATE utf8_unicode_ci AS `slug`,
                    `v`.`company_id` AS `company_id`,
                    CONCAT('Férias ', `v`.`name`) AS `name`,
                    `v`.`starting` AS `starting_date`,
                    `v`.`ending` AS `ending_date`
                FROM
                    `{$prefix}vacations` AS `v`
                WHERE 
                    `v`.`status` = 'approved'
                ORDER BY 
                    `v`.`starting` ASC)
                
                ORDER BY 
                    `starting_date` ASC, `ending_date` DESC;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $viewName = DB::getTablePrefix() . 'calendar';
        DB::statement("drop view {$viewName}");
    }
}