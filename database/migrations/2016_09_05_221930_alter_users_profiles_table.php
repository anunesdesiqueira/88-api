<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_profiles', function(Blueprint $table)
        {
            $table->dropColumn('gender');
        });

        Schema::table('users_profiles', function (Blueprint $table) {
            $table->enum('gender', ['male','female'])->default('male')->after('name');
            $table->date('date_of_birth')->nullable()->change();
            $table->string('identity_document', 9)->nullable()->change();
            $table->string('identity_document_state')->nullable()->change();
            $table->string('identity_document_dispatcher_organ')->nullable()->change();
            $table->date('identity_document_dispatcher_date')->nullable()->change();
            $table->string('register_individual', 11)->nullable()->change();
            $table->string('voter_id_card', 11)->nullable()->change();
            $table->string('driving_licence')->nullable()->change();
            $table->string('driving_licence_category')->nullable()->change();
            $table->date('driving_licence_dispatcher_date')->nullable()->change();
            $table->date('driving_licence_validate_date')->nullable()->change();
            $table->string('work_record_booklet')->nullable()->change();
            $table->string('work_record_booklet_series')->nullable()->change();
            $table->string('work_record_booklet_dispatcher_state')->nullable()->change();
            $table->string('social_integration_program')->nullable()->change();
            $table->text('professional_identity')->nullable()->change();
            $table->text('address')->nullable()->change();
            $table->text('data_address')->nullable()->change();
            $table->text('data_email')->nullable()->change();
            $table->text('data_phone')->nullable()->change();
            $table->text('data_phone_emergency')->nullable()->change();
            $table->text('about_me')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_profiles', function(Blueprint $table)
        {
            $table->dropColumn('gender');
        });

        Schema::table('users_profiles', function (Blueprint $table) {
            $table->enum('gender', ['male','female'])->default('male')->after('name');
            $table->date('date_of_birth')->change();
            $table->string('identity_document', 9)->change();
            $table->string('identity_document_state')->change();
            $table->string('identity_document_dispatcher_organ')->change();
            $table->date('identity_document_dispatcher_date')->change();
            $table->string('register_individual', 11)->change();
            $table->string('voter_id_card', 11)->change();
            $table->string('driving_licence')->change();
            $table->string('driving_licence_category')->change();
            $table->date('driving_licence_dispatcher_date')->change();
            $table->date('driving_licence_validate_date')->change();
            $table->string('work_record_booklet')->change();
            $table->string('work_record_booklet_series')->change();
            $table->string('work_record_booklet_dispatcher_state')->change();
            $table->string('social_integration_program')->change();
            $table->text('professional_identity')->change();
            $table->text('address')->change();
            $table->text('data_address')->change();
            $table->text('data_email')->change();
            $table->text('data_phone')->change();
            $table->text('data_phone_emergency')->change();
            $table->text('about_me')->change();
        });
    }

}
