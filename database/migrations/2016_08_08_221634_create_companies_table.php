<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('parent')->unsigned()->nullable();
            $table->integer('sector_id')->unsigned()->nullable();
            $table->integer('tax_framework_id')->unsigned()->nullable();
            $table->enum('type', ['headquarters', 'subsidiary'])->default('headquarters');
            $table->string('trading_name');
            $table->string('company_name')->nullable();
            $table->string('national_register_of_legal_entities');
            $table->string('state_registration')->nullable();
            $table->text('address');
            $table->text('data_address');
            $table->text('data_email');
            $table->text('data_phone');

            $table->index(['parent', 'type', 'trading_name', 'company_name']);

            $table->foreign('sector_id')->references('id')->on('sectors')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('tax_framework_id')->references('id')->on('taxes_frameworks')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('companies', function(Blueprint $table) {
            $table->foreign('parent')->references('id')->on('companies')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
