<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemunerationHasVariables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remuneration_has_variables', function(Blueprint $table) {
            $table->integer('remuneration_id')->unsigned();
            $table->integer('remuneration_variable_id')->unsigned();
            $table->string('value_or_amount');

            $table->foreign('remuneration_id')->references('id')->on('remunerations')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('remuneration_variable_id')->references('id')->on('remunerations_variables')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('remuneration_has_variables');
    }
}
