<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medias', function ($table) {
            $table->integer('created_user_id')->unsigned()->after('id');
            $table->integer('company_id')->nullable()->unsigned()->after('created_user_id');
            $table->string('name')->nullable()->after('slug');
            $table->enum('folder', ['company', 'team', 'personal'])->default('company')->after('name');
            $table->enum('access', ['public', 'private'])->default('public')->after('folder');

            $table->foreign('created_user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('company_id')->references('id')->on('companies')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medias', function ($table) {
            $table->dropForeign(['created_user_id']);
            $table->dropForeign(['company_id']);
            $table->dropColumn(['created_user_id', 'company_id', 'name', 'folder', 'access']);
        });
    }
}
