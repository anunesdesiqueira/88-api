<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPositionsJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('positions_jobs', function ($table) {
            $table->integer('company_id')->after('id')->nullable()->unsigned()->after('id');

            $table->foreign('company_id')->references('id')->on('companies')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('positions_jobs', function ($table) {
            $table->dropForeign('positions_jobs_company_id_foreign');
            $table->dropColumn(['company_id']);
        });
    }
}
