<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRefundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('refunds', function ($table) {
            $table->integer('created_user_id')->after('id')->unsigned();

            $table->foreign('created_user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('refunds', function ($table) {
            $table->dropForeign('refunds_created_user_id_foreign');
            $table->dropColumn(['created_user_id']);
        });
    }
}
