<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRemunerationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('remunerations', function ($table) {
            $table->dropForeign('remunerations_remuneration_variable_id_foreign');
            $table->dropColumn(['remuneration_variable_id', 'value_or_amount']);
        });

        Schema::table('remunerations', function ($table) {
            $table->string('name')->after('company_id');
            $table->date('payday')->after('name');
            $table->decimal('base_salary',15,2)->after('payday');
            $table->decimal('benefits',15,2)->after('base_salary');
            $table->decimal('discounts',15,2)->after('benefits');
            $table->decimal('net_salary',15,2)->after('discounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('remunerations', function ($table) {
            $table->dropColumn(['name', 'payday', 'base_salary', 'benefits', 'discounts', 'net_salary']);
        });

        Schema::table('remunerations', function ($table) {
            $table->integer('remuneration_variable_id')->unsigned();
            $table->string('value_or_amount');

            $table->foreign('remuneration_variable_id')->references('id')->on('remunerations_variables')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }
}
