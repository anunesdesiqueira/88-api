<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHourlyWorkloadHasUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hourly_workload_has_users', function(Blueprint $table) {
            $table->integer('hourly_workload_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('hourly_workload_id')->references('id')->on('hourly_workloads')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->primary(['hourly_workload_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hourly_workload_has_users');
    }
}
