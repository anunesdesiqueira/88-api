<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersAdditionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_additionals', function ($table) {
            $table->integer('vacation_balance_days')->unsigned()->default(0)->after('admission_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_additionals', function ($table) {
            $table->dropColumn(['vacation_balance_days']);
        });
    }
}
