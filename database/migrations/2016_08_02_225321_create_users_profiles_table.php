<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersProfilesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_profiles', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->enum('gender', ['male','female'])->default('male');
            $table->date('date_of_birth');
            $table->string('identity_document', 9)->unique(); //RG
            $table->string('identity_document_state'); //RG Estado
            $table->string('identity_document_dispatcher_organ'); //RG Orgão de Expeditor
            $table->date('identity_document_dispatcher_date'); //RG Data de Expedição
            $table->string('register_individual', 11)->unique(); //CPF
            $table->string('voter_id_card', 11)->unique(); //Titulo de Eleitor
            $table->string('driving_licence')->unique(); //CNH
            $table->string('driving_licence_category', 10); //CNH Categoria
            $table->date('driving_licence_dispatcher_date'); //CNH Data de Expedição
            $table->date('driving_licence_validate_date'); //CNH Data de Validate
            $table->string('work_record_booklet')->unique(); //Carteira de Trabalho
            $table->string('work_record_booklet_series'); //Serie Carteira de Trabalho
            $table->string('work_record_booklet_dispatcher_state'); //Estado Carteira de Trabalho
            $table->string('social_integration_program')->unique(); //PIS
            $table->text('professional_identity'); //Identidade Profissional
            $table->text('address');
            $table->text('data_address');
            $table->text('data_email');
            $table->text('data_phone');
            $table->text('data_phone_emergency');
            $table->text('about_me');

            $table->index(['name', 'gender', 'date_of_birth']);

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_profiles');
	}

}
