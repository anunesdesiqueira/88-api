<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('benefits', function ($table) {
            $table->integer('created_user_id')->after('id')->unsigned();

            $table->foreign('created_user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('benefits', function ($table) {
            $table->dropForeign('benefits_created_user_id_foreign');
            $table->dropColumn(['created_user_id']);
        });
    }
}
