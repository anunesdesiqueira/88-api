<?php

use Illuminate\Database\Seeder;

class SalaryTaxes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("salary_taxes")->delete();

        DB::table("salary_taxes")->insert([
            ['id' => 1, 'type' => 'inss', 'rules' => json_encode([
                ['starting_salary' => 0, 'ending_salary' => 1556.94, 'percentage' => 8],
                ['starting_salary' => 1556.95, 'ending_salary' => 2594.92, 'percentage' => 9],
                ['starting_salary' => 2594.93, 'ending_salary' => 5189.82, 'percentage' => 11],
                ['starting_salary' => 5189.82, 'discount' => 570.88],
            ]), 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 2, 'type' => 'irrf', 'rules' => json_encode([
                ['starting_salary' => 0, 'ending_salary' => 1903.98],
                ['starting_salary' => 1903.99, 'ending_salary' => 2826.65, 'percentage' => 7.5, 'discount' => 142.80],
                ['starting_salary' => 2826.66, 'ending_salary' => 3751.05, 'percentage' => 15, 'discount' => 354.80],
                ['starting_salary' => 3751.06, 'ending_salary' => 4664.68, 'percentage' => 22.5, 'discount' => 636.13],
                ['starting_salary' => 4664.68, 'percentage' => 27.5, 'discount' => 869.36],
            ]), 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
        ]);
    }
}
