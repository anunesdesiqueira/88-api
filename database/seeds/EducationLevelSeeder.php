<?php

use Illuminate\Database\Seeder;

class EducationLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("educations_levels")->delete();

        DB::table("educations_levels")->insert([
            ['id' => 1, 'name' => 'Superior Completo', 'slug' => 'superior_completo', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 2, 'name' => 'Superior Incompleto', 'slug' => 'superior_incompleto', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 3, 'name' => 'Ensino Médio', 'slug' => 'ensino_medio', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 4, 'name' => 'Ensino Fundamental', 'slug' => 'ensino_fundamental', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
        ]);
    }
}
