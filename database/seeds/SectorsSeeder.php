<?php

use Illuminate\Database\Seeder;

class SectorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("sectors")->delete();

        DB::table("sectors")->insert([
            ['id' => 1, 'name' => 'Indústria', 'slug' => 'industria', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 2, 'name' => 'Comércio', 'slug' => 'comercio', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 3, 'name' => 'Serviço', 'slug' => 'servico', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 4, 'name' => 'Outros', 'slug' => 'outros', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
        ]);
    }
}
