<?php

use Illuminate\Database\Seeder;

class DepartmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("departments")->delete();

        DB::table("departments")->insert([
            ['id' => 1, 'name' => 'Atendimento', 'slug' => 'atendimento', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 2, 'name' => 'Planejamento', 'slug' => 'planejamento', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 3, 'name' => 'Pesquisa', 'slug' => 'pesquisa', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 4, 'name' => 'Mídia', 'slug' => 'midia', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 5, 'name' => 'Criação', 'slug' => 'criacao', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 6, 'name' => 'Produção', 'slug' => 'producao', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
        ]);
    }
}
