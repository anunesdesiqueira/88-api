<?php

use Illuminate\Database\Seeder;

class CommemorativeDatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("commemorative_dates")->delete();

        DB::table("commemorative_dates")->insert([
            ['name' => 'Dia Internacional da Música', 'date' => '2016-10-01', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia do Vendedor', 'date' => '2016-10-01', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia do Vereador', 'date' => '2016-10-01', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia Internacional da Não-Violência', 'date' => '2016-10-02', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Eleições 2016', 'date' => '2016-10-02', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Primeiro Turno das Eleições 2016', 'date' => '2016-10-02', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia Mundial do Dentista', 'date' => '2016-10-03', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia das Abelhas', 'date' => '2016-10-03', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia dos Animais', 'date' => '2016-10-04', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia de São Francisco de Assis', 'date' => '2016-10-04', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia da Natureza', 'date' => '2016-10-04', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia do Cachorro', 'date' => '2016-10-04', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia de São Benedito', 'date' => '2016-10-05', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia de São Bruno', 'date' => '2016-10-06', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia do Compositor Brasileiro', 'date' => '2016-10-07', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia do Nordestino', 'date' => '2016-10-08', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia do Atletismo', 'date' => '2016-10-09', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['name' => 'Dia de São Daniel Comboni', 'date' => '2016-10-10', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
        ]);
    }
}
