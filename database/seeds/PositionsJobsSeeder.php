<?php

use Illuminate\Database\Seeder;

class PositionsJobsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("positions_jobs")->delete();

        DB::table("positions_jobs")->insert([
            ['id' => 1, 'name' => 'Diretor de Atendimento', 'slug' => 'diretor_de_atendimento', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 2, 'name' => 'Diretor de Criação', 'slug' => 'diretor_de_criacao', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 3, 'name' => 'Diretor de Mídia', 'slug' => 'diretor_de_midia', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 4, 'name' => 'Assistente de Atendimento', 'slug' => 'assistente_de_atendimento', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 5, 'name' => 'Chefe de Tráfego', 'slug' => 'chefe_de_trafego', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 6, 'name' => 'Redator', 'slug' => 'redator', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
        ]);
    }
}
