<?php

use Illuminate\Database\Seeder;

class TaxFrameworkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("taxes_frameworks")->delete();

        DB::table("taxes_frameworks")->insert([
            ['id' => 1, 'name' => 'Simples Nacional', 'slug' => 'simples_nacional', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 2, 'name' => 'Lucro Presumido', 'slug' => 'lucro_presumido', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 3, 'name' => 'Lucro Real', 'slug' => 'lucro_real', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
        ]);
    }
}
