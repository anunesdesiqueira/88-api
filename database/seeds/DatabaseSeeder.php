<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OAuthSeeder::class);
        $this->call(StatesSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(SectorsSeeder::class);
        $this->call(TaxFrameworkSeeder::class);
        $this->call(PositionsJobsSeeder::class);
        $this->call(DepartmentsSeeder::class);
        $this->call(EducationLevelSeeder::class);
        $this->call(TypeExpenditureSeeder::class);
        $this->call(ProvidersCategories::class);
        $this->call(BenefitsCategories::class);
        $this->call(EntrustSeeder::class);
        $this->call(CommemorativeDatesSeeder::class);
        $this->call(SalaryTaxes::class);
    }
}
