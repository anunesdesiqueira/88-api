<?php

use Illuminate\Database\Seeder;

class TypeExpenditureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("types_expenditures")->delete();

        DB::table("types_expenditures")->insert([
            ['id' => 1, 'name' => 'Estacionamento', 'slug' => 'estacionamento', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
        ]);
    }
}
