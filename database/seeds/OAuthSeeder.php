<?php

use Illuminate\Database\Seeder;

class OAuthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("oauth_clients")->delete();

        DB::table("oauth_clients")->insert([
            'id'            => 'd41d8cd98f00b204e9800998ecf8427e',
            'secret'        => 'horadoshow',
            'name'          => 'OitentaEOitoApi',
            'created_at'    => new DateTime(),
            'updated_at'    => new DateTime(),
        ]);
    }
}
