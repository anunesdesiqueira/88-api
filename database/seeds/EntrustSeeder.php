<?php

use Illuminate\Database\Seeder;
use OitentaOito\Entities\Role;
use OitentaOito\Entities\Permission;

class EntrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("roles")->delete();

        DB::table("roles")->insert([
            ['id' => 1, 'name' => 'admin', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 2, 'name' => 'team',  'created_at' => new DateTime(), 'updated_at' => new DateTime()],
        ]);
    }
}
