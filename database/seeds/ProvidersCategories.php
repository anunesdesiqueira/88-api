<?php

use Illuminate\Database\Seeder;

class ProvidersCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("providers_categories")->delete();

        DB::table("providers_categories")->insert([
            ['id' => 1, 'name' => 'Transporte', 'slug' => 'transporte', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 2, 'name' => 'Saúde', 'slug' => 'saude', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 3, 'name' => 'Alimentação', 'slug' => 'alimentacao', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
        ]);
    }
}
