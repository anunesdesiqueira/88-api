<?php

use Illuminate\Database\Seeder;

class BenefitsCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("benefits_categories")->delete();

        DB::table("benefits_categories")->insert([
            ['id' => 1, 'name' => 'Vale transporte', 'slug' => 'transporte', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 2, 'name' => 'Plano de Saúde', 'slug' => 'saude', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
            ['id' => 3, 'name' => 'Vale alimentação', 'slug' => 'alimentacao', 'created_at' => new DateTime(), 'updated_at' => new DateTime()],
        ]);
    }
}
